{{- $pathPrefix := (include "chart.pathPrefix" .) -}}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "chart.fullname" . }}
  labels:
    {{- include "chart.labels" . | nindent 4 }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount }}
  {{- end }}
  selector:
    matchLabels:
      {{- include "chart.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "chart.selectorLabels" . | nindent 8 }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "chart.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
      - name: {{ .Chart.Name }}
        securityContext:
          {{- toYaml .Values.securityContext | nindent 12 }}
        image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Chart.AppVersion }}"
        imagePullPolicy: {{ .Values.image.pullPolicy }}
        {{- if .Values.taskmanager.project.localVolume.enabled }}
        volumeMounts:
        - mountPath: {{ .Values.taskmanager.project.localVolume.podProjectPath }}
          name: {{ include "chart.fullname" . }}-local-volume
        {{- end }}
        envFrom:
        - configMapRef:
            name: {{ include "chart.fullname" . }}-configmap
        env:
          - name: DB_PASS
            valueFrom:
              secretKeyRef:
                name: {{ include "chart.fullname" . }}-secrets
                key: db-password
          - name: SECRET_KEY
            valueFrom:
              secretKeyRef:
                name: {{ include "chart.fullname" . }}-secrets
                key: secret-key
          - name: SECURITY_PASSWORD_SALT
            valueFrom:
              secretKeyRef:
                name: {{ include "chart.fullname" . }}-secrets
                key: security-password-salt
          {{- if and .Values.taskmanager.project.git.enabled .Values.taskmanager.project.git.key }}
          - name: SSH_PRIVATE_KEY_BASE64
            valueFrom:
              secretKeyRef:
                name: {{ include "chart.fullname" . }}-secrets
                key: project-repo-key
          {{- end }}
          {{-  if .Values.taskmanager.admin.password }}
          - name: TASKMAN_ADMIN_PASSWORD
            valueFrom:
              secretKeyRef:
                name: {{ include "chart.fullname" . }}-secrets
                key: TASKMAN_ADMIN_PASSWORD
          {{- end }}
        ports:
        - name: app-port
          containerPort: {{ .Values.taskmanager.port }}
          protocol: TCP
        livenessProbe:
          httpGet:
            path: {{ trimSuffix "/" $pathPrefix }}/-/healthy
            port: app-port
          timeoutSeconds: 2
          periodSeconds: 10
          failureThreshold: 12
        readinessProbe:
          httpGet:
            path: {{ trimSuffix "/" $pathPrefix }}/-/ready
            port: app-port
          timeoutSeconds: 2
          periodSeconds: 10
          failureThreshold: 12
        startupProbe:
          httpGet:
            path: {{ trimSuffix "/" $pathPrefix }}/-/ready
            port: app-port
          failureThreshold: 30
          periodSeconds: 5
        resources:
          {{- toYaml .Values.resources | nindent 12 }}
      {{- if .Values.taskmanager.project.localVolume.enabled }}
      volumes:
      - name: {{ include "chart.fullname" . }}-local-volume
        persistentVolumeClaim:
          claimName: {{ include "chart.fullname" . }}-local-pvc
      {{- end }}
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
