.PHONY: all build-all build help up down

APP_NAME:=$(shell grep "name=" setup.py | awk -F= '{gsub(/[ \t,\047]+/,""); print $$2}')
VERSION:=$(shell grep "VERSION[[:space:]]=" setup.py | awk -F= '{gsub(/[ \t\047]+/,""); print $$2}')
REGISTRY=registry.localhost:15000
IMAGE=${REGISTRY}/${APP_NAME}
TAG_ALIAS=develop

all: up build-all

up:
	$(MAKE) -C .k8s/k3d setup

down:
	$(MAKE) -C .k8s/k3d teardown

build-all: build tag-alias push

build: 
	$(info Building ${IMAGE}:${VERSION} ...)
	#docker build --pull -t "${IMAGE}:${VERSION}" -f .docker/Dockerfile .
	docker build --pull -t "${APP_NAME}:${VERSION}" -f .docker/Dockerfile .

tag-alias:
	docker tag ${APP_NAME}:${VERSION} ${APP_NAME}:${TAG_ALIAS}
	docker tag ${APP_NAME}:${VERSION} ${IMAGE}:${VERSION}
	docker tag ${IMAGE}:${VERSION} ${IMAGE}:${TAG_ALIAS}

push:
	docker push -a ${IMAGE}


#Helm install name
export HELM_NAME = task-manager
export USE_IMAGE=${IMAGE}
# The VALUES_FILE is relative to the ./charts directory
export VALUES_FILE='devvalues.yaml'
HELM_CHART_PATH = ./charts

# List of helm targets
HELM_TARGETS = all install install-dry upgrade upgrade-all upgrade-dry dep-build dep-update uninstall
# Add all helm targets with the "helm-" prefix.
HELM_TARGETS_PREFIX = $(addprefix helm-, $(HELM_TARGETS))
$(HELM_TARGETS_PREFIX):
	$(MAKE) -C $(HELM_CHART_PATH) $(subst helm-,,$@)

help:
	@echo '*** Usage of this file:'
	@echo 'make           : Setup local dev environment using k3d (see .k3s/k3d for configs) AND build and push all local images'
	@echo 'make up        : Setup local dev environmetn (k8s cluster and registry)'
	@echo 'make down      : Teardown local dev environment'
	@echo 'make build-all : Build and push the images to the local registry'
	@echo 'make build     : Build the image without pushing'
	@echo 'make tag-alias : Make a tag alias'
	@echo 'make push      : Push the images to the local registry'
	@echo
	@echo '*** Some helm related commands:'
	@echo 'make helm-all         : Install helm chart including updating and building dependencies'
	@echo 'make helm-install     : Install helm chart in the current kubectl context'
	@echo 'make helm-install-dry : Do a simulation of an installation'
	@echo 'make helm-upgrade     : Upgrade the helm chart as is'
	@echo 'make helm-upgrade-all : Upgrade the helm chart including updating and building dependencies'
	@echo 'make helm-upgrade-dry : Do a simulation of an upgrade'
	@echo 'make helm-dep-build   : Build the chart dependencies from the Chart.lock file values.yaml is not evaluated'
	@echo 'make helm-dep-update  : Refresh the and download the dependencies from the Chart.yaml file'
	@echo 'make helm-uninstall   : Uninstall the helm release'

