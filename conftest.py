# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
import pytest
import pathlib

from taskmanager import models
from taskmanager import create_app
from taskmanager.models import db
from taskmanager.util.helpers import get_object_from_arg

from taskmanager.tests.loaders import create_locks
from taskmanager.tests.loaders import create_random_test_tasks
from taskmanager.tests.loaders import create_random_test_taskgroup
from taskmanager.tests.loaders import create_test_tasks_all_users


# Add flag for functional tests
def pytest_addoption(parser):
    parser.addoption(
        "--database_uri", type=str, default='sqlite:///:memory:', help="Run functional tests (default=False)"
    )


@pytest.fixture(scope="session")
def db_uri(request):
    return request.config.getoption('--database_uri')


@pytest.fixture(scope="module")
def app(db_uri):
    """Create and configure a new app instance for each test."""
    # create the app with common test config
    app = create_app({
        'TESTING': True,
        'SQLALCHEMY_DATABASE_URI': db_uri,
        'SECURITY_PASSWORD_SALT': 'some_random_stuff',
        'SECRET_KEY': 'some_test_key'
    }, use_sentry=False)

    with app.app_context():
        yield app


@pytest.fixture(scope="function")
def init_db(app):
    # create the database and load test data
    db.create_all()
    yield app
    db.session.remove()
    db.drop_all()


@pytest.fixture(scope="session")
def test_data_dir():
    return pathlib.Path(__file__).parent / 'taskmanager' / 'tests'


@pytest.fixture(scope="function")
def app_config(test_data_dir, app, init_db):
    # Load the config file with initial setup
    config_file = test_data_dir / 'config' / 'test_config.json'
    from taskmanager.util.import_config import load_config_file
    load_config_file(app, config_file, silent=True)

    yield app


@pytest.fixture(scope="function")
def admin_config(test_data_dir, init_db):
    # Load the config file only with admin user to test config API
    import yaml
    from taskmanager.util.import_config import load_config

    config_file = test_data_dir / 'config' / 'test_admin_config.json'
    with open(config_file) as config_file:
        config_definition = yaml.safe_load(config_file.read())
    load_config(config_definition, silent=True)


@pytest.fixture(scope="function")
def random_test_data(test_data_dir, app, init_db, app_config):
    #TODO: now loading random tasks, which is not ideal. Data loading has to split 
    # out in multiple fixtures.
    base_tasks_path = test_data_dir / 'tasks'
    # Load random tasks 3 times
    create_random_test_tasks(app, base_tasks_path, silent=True)
    create_random_test_tasks(app, base_tasks_path, silent=True)
    create_random_test_tasks(app, base_tasks_path, silent=True)
    # Create a couple of random taskgroups
    create_random_test_taskgroup(app, num_tasks=2, silent=True)
    create_random_test_taskgroup(app, num_tasks=3, silent=True)
    create_random_test_taskgroup(app, num_tasks=4, silent=True)

    yield app

@pytest.fixture(scope="function")
def meta_task(test_data_dir, app, init_db, app_config):
    task_path = test_data_dir / 'tasks' / 'base_microbleeds.json'
    
    with app.app_context():
        raw_task_content = task_path.read_text()
        template = json.loads(raw_task_content)['template']
        template = get_object_from_arg(template, models.TaskTemplate, models.TaskTemplate.label)
        
        task = models.Task(
                       id = 9999,
                       project="test",
                       template=template,
                       raw_content=raw_task_content,
                       tracking_id = "exp001",
                       raw_callback_url="$SYSTEM_URL",
                       callback_content="http://localhost:5000/",
                       generator_url="https://localhost:5000/",
                       application_name="TaskR",
                       application_version="0.1.0")
        db.session.add(task)
        db.session.commit()


@pytest.fixture(scope="function")
def all_users_data(test_data_dir, app, init_db, app_config):
    base_tasks_path = test_data_dir / 'tasks'
    create_test_tasks_all_users(app, base_tasks_path, num_tasks=5, silent=True)
    yield app


@pytest.fixture(scope="function")
def all_users_one_task_data(test_data_dir, app, init_db, app_config):
    base_tasks_path = test_data_dir / 'tasks'
    create_test_tasks_all_users(app, base_tasks_path, num_tasks=1, silent=True)
    yield app


@pytest.fixture(scope="function")
def test_locks(app, init_db, app_config, all_users_one_task_data):
    create_locks(app)
    yield app


@pytest.fixture
def client(app):
    """A test client for the app."""
    # To add authentication, see: https://kite.com/python/docs/flask.current_app.test_client
    return app.test_client()


@pytest.fixture(scope="function")
def no_db_app():
    """Create and configure a new app instance for each test."""
    db_uri = 'sqlite:///some/non-existing/directory/that/cannot/be/created/database.sql'

    # create the app with common test config
    app = create_app({
        'TESTING': True,
        'SQLALCHEMY_DATABASE_URI': db_uri,
    }, use_sentry=False)

    with app.app_context():
        yield app


@pytest.fixture(scope="function")
def no_db_client(no_db_app):
    """A test client for the app."""
    return no_db_app.test_client()
