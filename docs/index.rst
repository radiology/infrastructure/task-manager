***********
TaskManager
***********

The TaskManager manages tasks for manual interaction with image and derived data.
Tasks can be added to the TaskManager by the
`StudyGovernor <https://study-governor.readthedocs.io>`_ via its REST API but can
also be used stand-alone. The TaskManager is part of the Imaging Data Science Platform,
an infrastructure for population and cohort based medical imaging studies developed by
the Erasmus MC.

The official documentation can be found at
`task-manager.readthedocs.io <https://task-manager.readthedocs.io>`_


The TaskManager is open-source (licensed under the Apache 2.0 license) and hosted
on Gitlab at `<https://gitlab.com/radiology/infrastructure/task-manager>`_


TaskManager documentation
=============================
.. toctree::
    :maxdepth: 3

    static/introduction.rst
    static/tutorial.rst
    static/howto.rst
    static/manual.rst
    static/api.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
