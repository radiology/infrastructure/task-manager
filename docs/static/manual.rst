******
Manual
******

==========================================
TaskManager commands from the command line
==========================================

Another way to interact with the TaskManager is via a set of scripts/executables
available in the TaskManager environment (folders) after installing on a Windows
or Linux server. These executables are especially useful if you want to upload or
change a large number of tasks and/or templates. In that case, the REST API is
not very convenient since actions/commands are given one by one.

Available and useful commands that can be used from the Windows or Linux command
line box are shown below. Note that arguments between square brackets are optional.

--------------------
taskmanager-add-task
--------------------

Usage::

    taskmanager-add-task [-h] [-db] -p PROJECT [-u USERS [USERS ...]]
        [-g GROUPS [GROUPS ...]] [-t TAGS [TAGS ...]] [-d DISTRIBUTE_GROUP]
        [-gen GENERATOR_URL] [-an APPLICATION_NAME] [-av APPLICATION_VERSION]
        [-cu CALLBACK_URL] [-cc CALLBACK_CONTENT] [--url URL] [--old] task

Example::

    taskmanager-add-task -p glandula -u johndoe -t pinealis --url 10.20.30.40 --old task.json

By executing the example command above, a new task (JSON file task.json) is
uploaded to the TaskManager running on URL 10.20.30.40. HOSTNAME can be a IP
address or a server name known in the domain. The project related task is
"glandula" and the specific user the task is assigned to is "johndoe". The ``-t``
option is important here, since it means that this task, when carried out on
the ViewR will use template ‘pinealis’. So when uploading the task to the
TaskManager it is immediately linked to the task_template (ViewR template)
that has to be used whule running the task. The –old option here is used
because of a software version issue.

If the task would need to be assigned to a group of users (say with the name
phds), so anyone of them can pick the task up to carry out, you should give
the following::

    taskmanager-add-task -g phds -u johndoe -t pinealis --url 10.20.30.40 --old task.json

--------------------
taskmanager-add-user
--------------------

Usage::

    taskmanager-add-user [-h] -u USERNAME -p PASSWORD -n FULL_NAME -e EMAIL
        [-w ASSIGNMENT_WEIGHT] [-i] [-f]

This command adds a new user to the Task Manager. Most arguments will be
trivial/clear. The argument ``-h`` or ``--help`` will show the help message and
exit. The (optional) argument ``-I`` means that the new user should be
‘inactive’. The (optional) argument ``-f`` means that execution is forced
without asking questions.

------------------------
taskmanager-add-template
------------------------

Usage::

    taskmanager-add-template [-h] [--old] HOSTNAME template

Example::

    taskmanager-add -add --old http://10.20.30.40:5000 template.json

By executing the example command above, a new template (JSON file template.json)
will be uploaded to the Task Manager running on URL ``http://10.20.30.40`` on
port ``5000``.

---------------------------
Taskmanager-update-template
---------------------------

Usage::

    taskmanager-update-template [-h] [--name NAME] [--user USER] [--old]
        HOSTNAME TEMPLATE.JSON

For example::

    taskmanager-update-add --old http://10.20.30.40:5000 template.json

This command will update an existing template (known to and stored on the Task Manager).

.. _configuration-section:

=============
Configuration
=============

Most of the configuration of the app uses environment variables. The TaskManager uses
the following configuration variables:

===========================  ==================================================
Environment variable         Description
===========================  ==================================================
TASKMANAGER_PORT             Port on which the TaskManager will isten
TASKMAN_INSTANCE_NAME        Instance name of the service, default ``Task Manager``
TASKMAN_PROJECT_NAME         Project anme for the task manager, default ``DefaultProject``
DB_SCHEMA                    Database schema to use, see `SQLAlchemy Engine Configuration <https://docs.sqlalchemy.org/en/20/core/engines.html>`_
DB_NAME                      Name of the database to use
DB_USER                      User to use to connect to the database
DB_PASS                      Password to connect to the database
DB_HOST                      Hostname of the database server
DB_PORT                      Port of the databse server
SECURITY_REGISTERABLE        Flag indicating if users are allowed to register themselves
SECURITY_CHANGEABLE          Flag indicating if users are allowed to change their passwords
SECURITY_RECOVERABLE         Flag indicating if users are allowed to recover their passwords
SECURITY_CONFIRMABLE         Flag indicating if users need to confirm their email before registration is complete
SECURITY_PASSWORD_SALT       Salt to use for the password hashing
SECRET_KEY                   The Flask `secret key <https://flask.palletsprojects.com/en/3.0.x/config/#SECRET_KEY>`_
===========================  ==================================================

If use the TaskManager via docker-compose or kubernetes, you can automatically have it pull
project data from a GIT repository. For that set the following environment variables:

===========================  ==================================================
Environment variable         Description
===========================  ==================================================
PROJECT_REPO                 The GIT repository from where to get the project resources
PROJECT_BRANCH               The GIT branch from which to get the project resources
PROJECT_RELATIVE_PATH        The relative path inside the container in which to put the project resources
===========================  ==================================================

The following Gunicorn environment variables might be of interest:

===========================  ==================================================
Environment variable         Description
===========================  ==================================================
GUNICORN_WORKER_CLASS        `Gunicorn work class <https://docs.gunicorn.org/en/stable/settings.html?highlight=env%20#worker-class>`_
GUNICORN_WORKER_CONNECTIONS  `Gunicorn work connection <https://docs.gunicorn.org/en/stable/settings.html?highlight=env%20#worker-connections>`_
GUNICORN_TIMEOUT             `Gunicorn timeout <https://docs.gunicorn.org/en/stable/settings.html?highlight=env%20#timeout>`_
===========================  ==================================================

For the full Gunicorn settings see https://docs.gunicorn.org/en/stable/settings.html

=====================
Permissions and Roles
=====================

-----
Roles
-----

For the TaskManager the roles can be configured, but by default the following
roles are defined:

========= =================================================
Name      Permissions
========= =================================================
admin     ``task_read_all``,
          ``task_update_all``,
          ``task_add``,
          ``task_delete``,
          ``template_add``,
          ``template_update``,
          ``template_delete``,
          ``tag_add``,
          ``tag_update``,
          ``tag_delete``,
          ``user_read_all``,
          ``user_update_all``,
          ``user_add``,
          ``user_delete``,
          ``group_read_all``,
          ``group_add``,
          ``group_update``,
          ``group_delete``,
          ``meta_read_all``,
          ``meta_update``,
          ``meta_add``,
          ``meta_delete``,
          ``roles_manage``
superuser ``task_read_all``,
          ``task_update_status_all``,
          ``task_update_user``,
          ``task_update_lock_all``,
          ``tag_add``,
          ``tag_update``,
          ``user_read_all``,
          ``user_update_assignment_weight``,
          ``group_read_all``,
          ``group_update``
user      ``task_read``,
          ``task_update_status``,
          ``user_read``,
          ``group_read``
========= =================================================


-----------
Permissions
-----------


The permissions are defined as follows:

=================================   =================================================================================================================================
Permission                          Description
=================================   =================================================================================================================================
``task_read``                       Allows reading tasks assigned to itself. Please note: acquiring and releasing the lock field is part of this permission.
``task_read_all``                   Allows reading all tasks.
``task_update_status``              Allows updating the status of tasks assigned to itself.
``task_update_status_all``          Allows updating the status of all tasks.
``task_update_user``                Allows updating the assigned users and/or groups of tasks.
``task_update_lock_all``            Allows updating or clear the lock of all tasks.
``task_update_all``                 Allows updating all fields of all tasks.
``task_add``                        Allows adding a task.
``task_delete``                     Allows deleting a task.
``template_add``                    Allows adding a template.
``template_update``                 Allows updating templates.
``template_delete``                 Allows deleting a template.
``tag_add``                         Allows adding a tag.
``tag_update``                      Allows updating a tag.
``tag_delete``                      Allows deleting a tag.
``user_read``                       Allows seeing your user information.
``user_read_all``                   Allows seeing all users.
``user_add``                        Allows adding users.
``user_update_all``                 Allows updating all users.
``user_update_assignment_weight``   Allows changing the assignment weight of users.
``user_delete``                     Allows deleting users.
``group_read``                      Allows seeing the groups you are assigned to.
``group_read_all``                  Allows seeing all groups.
``group_add``                       Allows adding groups.
``group_update``                    Allows updating groups. Please note: this is how user membership of a group can be managed.
``group_delete``                    Allows deleting groups.
``roles_manage``                    Allows adding and removing roles from users
=================================   =================================================================================================================================


.. _Model:

===============
Database Models
===============

.. figure:: images/er_diagram_taskman.png
   :scale: 50%
   :alt: Database model for the TaskManager tables

   Database model of the TaskManager tables.

