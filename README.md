# TaskManager

The TaskManager keeps track of lists of tasks that users should perform. It
has a REST interface to access tasks and update them.

## Documentation

The TaskManager is documented at https://task-manager.readthedocs.io/

## Deployment

The TaskManager deployment typically uses either Kubernetes or
docker-compose. Documentation on how to deploy the TaskManager
can be found in the deployment section in the documentation at
https://task-manager.readthedocs.io/en/latest/static/howto.html#deployment

## Source Code

The TaskManager is open-source and licensed under an Apache 2.0 License.
The source code found be found at GitLab: https://gitlab.com/radiology/infrastructure/task-manager/