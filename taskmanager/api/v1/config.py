# Copyright 2017-2021 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import datetime

import yaml
from flask import abort
from flask_restx import fields, reqparse, Resource
from flask_security import http_auth_required, roles_accepted

from .base import api
from ... import models, db
from ...fields import ObjectUrl
from ...util.import_config import load_config


db = models.db

role_summary = api.model("RoleSummary", {
    'id': fields.Integer,
    'name': fields.String,
    'uri': fields.Url('api_v1.role')
})


user_summary = api.model("UserSummary", {
    'id': fields.Integer,
    'username': fields.String,
    'name': fields.String,
    'uri': fields.Url('api_v1.user'),
    'assignment_weight': fields.Float,
    'email': fields.String
})


groups_summary = api.model("GroupSummary", {
    'name': fields.String,
    'groupname': fields.String
})


config_load = api.model("Config", {
    'updated_at': fields.DateTime(dt_format='iso8601'),
    'roles': fields.List(fields.Nested(role_summary)),
    'users': fields.List(fields.Nested(user_summary)),
    'groups': fields.List(fields.Nested(groups_summary)),
    'tags': fields.List(fields.String),
})


@api.route('/service/config/import-config', endpoint='config')
class ConfigAPI(Resource):
    request_parser = reqparse.RequestParser()
    request_parser.add_argument('config_definition',
                                type=str,
                                required=True,
                                location='json',
                                help='No label specified')

    @http_auth_required
    @roles_accepted('admin')
    @api.expect(request_parser)
    @api.marshal_with(config_load)
    @api.response(201, "Created group")
    def post(self):
        args = self.request_parser.parse_args()
        config_definition = args.get('config_definition')

        try:
            config_definition = yaml.safe_load(config_definition)
        except yaml.YAMLError as exc:
            abort(400, f'Config definition is not a valid YAML file. {exc}')

        try:
            created_objects = load_config(config_definition)
            db.session.commit()

            result = {
                'updated_at': datetime.datetime.now(),
                **created_objects
            }

            return result, 201
        except:
            abort(400, 'Config definition is invalid.')

    @http_auth_required
    @roles_accepted('admin')
    @api.expect(request_parser)
    @api.marshal_with(config_load)
    @api.response(200, "Updated")
    def put(self):
        args = self.request_parser.parse_args()
        config_definition = args.get('config_definition')

        try:
            config_definition = yaml.safe_load(config_definition)
        except yaml.YAMLError as exc:
            abort(400, f'Config definition is not a valid YAML file. {exc}')

        try:
            created_objects = load_config(config_definition, overwrite=True)
            db.session.commit()

            result = {
                'updated_at': datetime.datetime.now(),
                **created_objects
            }

            return result, 200
        except:
            abort(400, f'Config definition is invalid')
