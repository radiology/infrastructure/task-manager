# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from flask import url_for


def test_healthy(client):
    response = client.get(url_for('health_api.healthy'))
    assert response.status_code == 200

    data = response.get_json()
    assert data is None


def test_ready(client, app_config):
    response = client.get(url_for('health_api.ready'))
    assert response.status_code == 200

    data = response.get_json()
    assert data is None


def test_not_ready(no_db_client):
    # Because the application is in a non ready state we cannot use url_for to resolve the uri
    response = no_db_client.get('/-/ready')
    print(response.get_json())
    assert response.status_code == 500

    data = response.get_json()
    assert data is None


def test_version(client):
    response = client.get(url_for('health_api.versions'))
    assert response.status_code == 200

    data = response.get_json()
    assert 'version' in data
    assert 'api_versions' in data

