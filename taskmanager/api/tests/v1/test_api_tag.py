# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import re

from flask import url_for
from flask_security.utils import hash_password

from taskmanager.tests.helpers import basic_auth_authorization_header

TIMESTAMP_REGEX = r'\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d'


def test_tags_get_as_admin(client, random_test_data):
    """ An admin user should be able to get the tags from a task. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.usertasks', id=3), headers=headers)
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list)
    assert 'status' in data['tasks'][4]
    assert 'tags' in data['tasks'][4]
    print(data['tasks'][4])
  
  
def test_tags_get_as_superuser(client, random_test_data):
    """ An admin user should be able to get the tags from a task. """
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.usertasks', id=3), headers=headers)
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list)
    assert 'status' in data['tasks'][4]
    assert 'tags' in data['tasks'][4]
    print(data['tasks'][4])


def test_tags_get_as_user(client, random_test_data):
    """ A user should be able to get the tags from a task assigned to him/her. """
    headers = basic_auth_authorization_header('user1', 'user1')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.usertasks', id=3), headers=headers)
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list)
    assert 'status' in data['tasks'][4]
    assert 'tags' in data['tasks'][4]
    print(data['tasks'][4])


def test_tags_get_all_as_admin(client, random_test_data):
    """ An admin user should be able to get an overview of all. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    
    for x in range(1,4):
        response = client.get(url_for('api_v1.tag', id=x), headers=headers)
        assert response.status_code == 200
        data = response.get_json()
        print(data)


def test_tags_get_all_as_superuser(client, random_test_data):
    """ A superuser should be able to get an overview of all. """
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    
    for x in range(1,4):
        response = client.get(url_for('api_v1.tag', id=x), headers=headers)
        assert response.status_code == 200
        data = response.get_json()
        print(data)


def test_tags_get_all_as_user(client, random_test_data):
    """ A user should be able to get an overview of all. """
    #TODO Is that really what we wish?
    headers = basic_auth_authorization_header('user1', 'user1')
    headers.update({'accept': 'application/json'})
    
    for x in range(1,4):
        response = client.get(url_for('api_v1.tag', id=x), headers=headers)
        assert response.status_code == 200
        data = response.get_json()
        print(data)


def test_get_tasks_with_tag_as_admin(client, random_test_data):
    """ An admin user should be able to get an overview of all tasks with a specific tag. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    
    response = client.get(url_for('api_v1.tagtasks', id=1), headers=headers)
    assert response.status_code == 200
    data = response.get_json()
    print(data)
    
    
def test_get_tasks_with_tag_as_superuser(client, random_test_data):
    """ A superuser should be able to get an overview of all tasks with a specific tag. """
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    
    response = client.get(url_for('api_v1.tagtasks', id=2), headers=headers)
    assert response.status_code == 200
    data = response.get_json()
    print(data)
    
    
def test_get_tasks_with_tag_as_user(client, random_test_data):
    """ A user should be able to get an overview of all tasks with a specific tag. """
    headers = basic_auth_authorization_header('user2', 'user2')
    headers.update({'accept': 'application/json'})
    
    response = client.get(url_for('api_v1.tagtasks', id=3), headers=headers)
    assert response.status_code == 200
    data = response.get_json()
    print(data)
    
    
def test_tag_post_new_tag_as_admin(client, random_test_data):
    """ An admin should be able to post a new tag. """
    body = {
        "name": 'TagNumber4',
    }
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    
    response = client.post(url_for('api_v1.tags'), json=body, headers=headers)
    assert response.status_code == 201

    response = client.get(response.get_json()['uri'], headers=headers)
    data = response.get_json()
    assert body["name"] == data["name"]
    #TODO posting of tags seem to be possible, putting not ???


def test_tag_post_new_tag_as_superuser(client, random_test_data):
    """ A superuser should be able to post a new tag. """
    body = {
        "name": 'TagNumber5',
    }
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})

    response = client.post(url_for('api_v1.tags'), json=body, headers=headers)
    assert response.status_code == 201

    response = client.get(response.get_json()['uri'], headers=headers)
    data = response.get_json()
    assert body["name"] == data["name"]


def test_tag_post_new_tag_as_user(client, random_test_data):
    """ A user should not be able to post a new tag. """
    body = {
        "name": 'TagNumber6',
    }
    headers = basic_auth_authorization_header('user1', 'user1')
    headers.update({'accept': 'application/json'})
    
    response = client.post(url_for('api_v1.tags'), json=body, headers=headers)
    assert response.status_code == 403


def test_tag_delete_tag_as_admin(client, random_test_data):
    """ An admin should be able to delete a tag. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    
    # Create tag
    body = {
        "name": 'TestTag',
    }
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    
    response = client.post(url_for('api_v1.tags'), json=body, headers=headers)
    assert response.status_code == 201

    # Check if the tag can be deleted
    response = client.delete(url_for('api_v1.tag', id=1), headers=headers)
    assert response.status_code == 200

    response = client.get(url_for('api_v1.tag', id=1), headers=headers)
    assert response.status_code == 404
    data = response.get_json()
    print(data)


def test_tag_delete_tag_as_superuser(client, random_test_data):
    """ A superuser should not be able to delete a tag. """
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    
    response = client.delete(url_for('api_v1.tag', id=5), headers=headers)
    assert response.status_code == 403


def test_tag_delete_tag_as_user(client, random_test_data):
    """ A user should not be able to delete tag. """
    headers = basic_auth_authorization_header('user1', 'user1')
    headers.update({'accept': 'application/json'})
    
    response = client.delete(url_for('api_v1.tag', id=5), headers=headers)
    assert response.status_code == 403


def test_tag_put_as_admin(client, app_config):
    """ Test if the admin can put a tag to a specific task. This should be allowed. """
    body = {
        "name": 'Testing',
    }
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.put(url_for('api_v1.tag', id=1), json=body, headers=headers)
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    # Check if the data is correct in the response.
    assert data["name"] == body["name"]
    #TODO are there more datafields that can be set in group?


def test_tag_put_as_superuser(client, app_config):
    """ Test if the superuser can put a tag to a specific task. This should be allowed. """
    body = {
        "name": 'Testing',
    }
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    response = client.put(url_for('api_v1.tag', id=1), json=body, headers=headers)
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    # Check if the data is correct in the response.
    assert data["name"] == body["name"]
    #TODO are there more datafields that can be set in group?
    
def test_tag_put_as_user(client, app_config):
    """ Test if a user can put a tag to a specific task. This should not be allowed. """
    body = {
        "name": "Testing",
    }
    headers = basic_auth_authorization_header('user1', 'user1')
    headers.update({'accept': 'application/json'})
    response = client.put(url_for('api_v1.tag', id=1), json=body, headers=headers)
    assert response.status_code == 403


def test_tag_put_nonexisting_tag_as_admin(client, app_config):
    """ Test if the admin can put a nonexisting tag to a specific task. This should not be allowed. """
    pass
    #TODO This should be moved to the test_api_task tests
    #TODO are there more datafields that can be set in group?
    # tag 2 seems to be not present. not found. 
    # tag 1 (Testing) is not changed with this put!


def test_tag_put_nonexisting_tag_as_superuser(client, app_config):
    """ Test if the superuser can put a nonexisting tag to a specific task. This should be not allowed. """
    pass
    #TODO This should be moved to the test_api_task tests
    #TODO are there more datafields that can be set in group?
    

def test_tag_put_nonexisting_tag_as_user(client, app_config):
    """ Test if a user can put a nonexisting tag to a specific task. This should be not allowed. """
    pass
    #TODO This should be moved to the test_api_task tests
    #TODO are there more datafields that can be set in group?
