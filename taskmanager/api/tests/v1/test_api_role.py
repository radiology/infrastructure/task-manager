# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import re

from flask import url_for
from flask_security.utils import hash_password

from taskmanager.tests.helpers import basic_auth_authorization_header

TIMESTAMP_REGEX = r'\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d'


def test_role_get_as_admin(client, random_test_data):
    """ An admin user should be able to get the tags from a task. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.roles', id=3), headers=headers)
    assert response.status_code == 200

#    data = response.get_json()
#    assert 'tasks' in data
#    assert isinstance(data['tasks'], list)
#    assert 'status' in data['tasks'][4]
#    assert 'tags' in data['tasks'][4]
#    print(data['tasks'][4])
  
  
def test_role_get_as_superuser(client, random_test_data):
    """ An admin user should be able to get the tags from a task. """
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.roles', id=2), headers=headers)
    assert response.status_code == 403

#    data = response.get_json()
#    assert 'tasks' in data
#    assert isinstance(data['tasks'], list)
#    assert 'status' in data['tasks'][4]
#    assert 'tags' in data['tasks'][4]
#    print(data['tasks'][4])


def test_role_get_as_user(client, random_test_data):
    """ A user should be able to get the tags from a task assigned to him/her. """
    headers = basic_auth_authorization_header('user1', 'user1')
    headers.update({'accept': 'application/json'})
    response = client.get(url_for('api_v1.roles', id=1), headers=headers)
    assert response.status_code == 403

#    data = response.get_json()
#    assert 'tasks' in data
#    assert isinstance(data['tasks'], list)
#    assert 'status' in data['tasks'][4]
#    assert 'tags' in data['tasks'][4]
#    print(data['tasks'][4])


def test_roles_get_all_as_admin(client, random_test_data):
    """ An admin user should be able to get an overview of all. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    
    for x in range(1,4):
        response = client.get(url_for('api_v1.role', id=x), headers=headers)
        assert response.status_code == 200
        data = response.get_json()
        print(data)


def test_roles_get_all_as_superuser(client, random_test_data):
    """ A superuser should be able to get an overview of all. """
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    
    for x in range(1,4):
        response = client.get(url_for('api_v1.role', id=x), headers=headers)
        assert response.status_code == 403
        data = response.get_json()
        print(data)


def test_roles_get_all_as_user(client, random_test_data):
    """ A user should be able to get an overview of all. """
    #TODO Is that really what we wish?
    headers = basic_auth_authorization_header('user1', 'user1')
    headers.update({'accept': 'application/json'})
    
    for x in range(1,4):
        response = client.get(url_for('api_v1.role', id=x), headers=headers)
        assert response.status_code == 403
        data = response.get_json()
        print(data)
