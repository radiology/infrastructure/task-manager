# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import re

from flask import url_for
from flask_security.utils import hash_password

from taskmanager.tests.helpers import basic_auth_authorization_header

TIMESTAMP_REGEX = r'\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d'


def test_lock_get_as_admin(client, test_locks):
    """ An admin user should be able to get the lock on a task. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})

    # Get the lock of anothers users task
    response = client.get(url_for('api_v1.usertasks', id=3), headers=headers)
    task_id = response.get_json()['tasks'][0]['id']

    response = client.get(url_for('api_v1.lock', id=task_id), headers=headers)
    assert response.status_code == 200

  
def test_lock_get_as_superuser(client, test_locks):
    """ A superuser should be able to get the locks on a task. """
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})

    # Get the lock of anothers users task
    response = client.get(url_for('api_v1.usertasks', id=4), headers=headers)
    task_id = response.get_json()['tasks'][0]['id']

    response = client.get(url_for('api_v1.lock', id=task_id), headers=headers)
    assert response.status_code == 200


def test_lock_get_as_user(client, test_locks):
    """ A user should be able to get the lock on its own task. """
    headers = basic_auth_authorization_header('user1', 'user1')
    headers.update({'accept': 'application/json'})

    # Get the task_id of the first task of the current user.
    response = client.get(url_for('api_v1.usertasks', id=3), headers=headers)
    task_id = response.get_json()['tasks'][0]['id']

    response = client.get(url_for('api_v1.lock', id=task_id), headers=headers)
    assert response.status_code == 200


def test_lock_put_as_admin(client, all_users_one_task_data):
    """ An admin should be able to update the lock on a task. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})

    # Get the task_id of the first task of the user we want to lock a task of
    response = client.get(url_for('api_v1.usertasks', id=3), headers=headers)
    task_id = response.get_json()['tasks'][0]['id']

    body = {"lock": 'user1'}
    response = client.put(url_for('api_v1.lock', id=task_id), json=body, headers=headers)
    assert response.status_code == 200

  
def test_lock_put_as_superuser(client, all_users_one_task_data):
    """ An admin user should be able to get the tags from a task. """
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})

    # Get the task_id of the first task of the user we want to lock a task of
    response = client.get(url_for('api_v1.usertasks', id=4), headers=headers)
    task_id = response.get_json()['tasks'][0]['id']

    body = {"lock": 'user2'}
    response = client.put(url_for('api_v1.lock', id=task_id), json=body, headers=headers)
    assert response.status_code == 200


def test_lock_put_as_superuser_on_itself(client, all_users_one_task_data):
    """ An admin user should be able to get the tags from a task. """
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})

    # Get the task_id of the first task of the user we want to lock a task of
    response = client.get(url_for('api_v1.usertasks', id=4), headers=headers)
    task_id = response.get_json()['tasks'][0]['id']

    body = {"lock": 'superuser'}
    response = client.put(url_for('api_v1.lock', id=task_id), json=body, headers=headers)
    assert response.status_code == 200


def test_lock_put_as_user(client, all_users_one_task_data):
    """ A user should be able to get the tags from a task assigned to him/her. """
    user = "halfuser1"
    headers = basic_auth_authorization_header(user, user)
    headers.update({'accept': 'application/json'})

    # Get the task_id of the first task of the user we want to lock a task of
    response = client.get(url_for('api_v1.usertasks', id=5), headers=headers)
    task_id = response.get_json()['tasks'][0]['id']

    body = {"lock": user}
    response = client.put(url_for('api_v1.lock', id=task_id), json=body, headers=headers)
    assert response.status_code == 200


def test_lock_put_as_user_empty_arg(client, all_users_one_task_data):
    """ A user should be able to get the tags from a task assigned to him/her. """
    user = "halfuser1"
    headers = basic_auth_authorization_header(user, user)
    headers.update({'accept': 'application/json'})

    # Get the task_id of the first task of the user we want to lock a task of
    response = client.get(url_for('api_v1.usertasks', id=5), headers=headers)
    task_id = response.get_json()['tasks'][0]['id']

    body = {"lock": None}
    response = client.put(url_for('api_v1.lock', id=task_id), json=body, headers=headers)
    assert response.status_code == 200
    assert response.get_json()['lock'] == "/api/v1/users/5"


def test_lock_delete_as_admin(client, test_locks):
    """ An admin user should be able to get the tags from a task. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})

    # Get the task_id of the first task of the user we want to lock a task of
    response = client.get(url_for('api_v1.usertasks', id=1), headers=headers)
    task_id = response.get_json()['tasks'][0]['id']

    response = client.delete(url_for('api_v1.lock', id=task_id), headers=headers)
    assert response.status_code == 200

    response = client.get(url_for('api_v1.lock', id=task_id), headers=headers)
    assert response.get_json()['lock'] is None
  
  
def test_lock_delete_as_superuser(client, test_locks):
    """ An admin user should be able to get the tags from a task. """
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})

    # Get the task_id of the first task of the user we want to lock a task of
    response = client.get(url_for('api_v1.usertasks', id=2), headers=headers)
    task_id = response.get_json()['tasks'][0]['id']

    response = client.delete(url_for('api_v1.lock', id=task_id), headers=headers)
    assert response.status_code == 200

    response = client.get(url_for('api_v1.lock', id=task_id), headers=headers)
    assert response.get_json()['lock'] is None


def test_lock_delete_as_user(client, test_locks):
    """ A user should be able to get the tags from a task assigned to him/her. """
    headers = basic_auth_authorization_header('user1', 'user1')
    headers.update({'accept': 'application/json'})

    # Get the task_id of the first task of the user we want to lock a task of
    response = client.get(url_for('api_v1.usertasks', id=3), headers=headers)
    task_id = response.get_json()['tasks'][0]['id']

    response = client.delete(url_for('api_v1.lock', id=task_id), headers=headers)
    assert response.status_code == 200

    response = client.get(url_for('api_v1.lock', id=task_id), headers=headers)
    assert response.get_json()['lock'] is None