# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import re

from flask import url_for

from taskmanager.tests.helpers import basic_auth_authorization_header

TIMESTAMP_REGEX = r'\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d'


def test_groups_get_as_admin(client, app_config):
    """ An admin user should be able to get the list of groups. """
    response = client.get(url_for('api_v1.groups'), headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'groups': [url_for('api_v1.group', id=x) for x in range(1, 4)]}


def test_groups_get_as_superuser(client, app_config):
    """ A superuser should be able to get the list of groups. """
    response = client.get(url_for('api_v1.groups'), headers=basic_auth_authorization_header("superuser", "superuser"))
    assert response.status_code == 200

    data = response.get_json()
    assert data == {'groups': [url_for('api_v1.group', id=x) for x in range(1, 4)]}


def test_groups_get_as_user(client, app_config):
    """ A normal user should NOT be able to get the list of groups. """
    response = client.get(url_for('api_v1.groups'), headers=basic_auth_authorization_header("user1", "user1"))
    assert response.status_code == 403
    #TODO: test if the body does not contain important information


def test_group_get_self_as_admin(client, app_config):
    """ An admin user should be able to retrieve it's own group information. """
    response = client.get(url_for('api_v1.group', id=1), headers=basic_auth_authorization_header('admin', 'admin'))
    assert response.status_code == 200

    data = response.get_json()

    # Check if create_time is in the response
    assert 'create_time' in data
    # get the create_time out if the response
    create_time = data.pop('create_time')

    # Check if time is a valid time format
    assert re.match(TIMESTAMP_REGEX, create_time) is not None

    # Test if everything is there expect for create_time
    assert data == {
        "groupname": "neuroradiologists",
        "uri": "/api/v1/groups/1",
        "name": "Neuroradiologists",
        "tasks": "/api/v1/groups/1/tasks",
        "users": "/api/v1/groups/1/users"
      }

def test_group_get_other_as_admin(client, app_config):
    """ An admin should be able to get any group information. """
    response = client.get(url_for('api_v1.group', id=1), headers=basic_auth_authorization_header('admin', 'admin'))
    assert response.status_code == 200

    data = response.get_json()

    # Check if create_time is in the response
    assert 'create_time' in data
    # get the create_time out if the response
    create_time = data.pop('create_time')

    # Check if time is a valid time format
    assert re.match(TIMESTAMP_REGEX, create_time) is not None

    # Test if everything is there expect for create_time
    assert data == {
        "groupname": "neuroradiologists",
        "uri": "/api/v1/groups/1",
        "name": "Neuroradiologists",
        "tasks": "/api/v1/groups/1/tasks",
        "users": "/api/v1/groups/1/users"
      }


def test_group_get_nonexisting_as_admin(client, app_config):
    """ An admin user should get a 404 (not found) when asking for a non existing group. """
    response = client.get(url_for('api_v1.group', id=10000), headers=basic_auth_authorization_header('admin', 'admin'))
    assert response.status_code == 404
    

def test_group_get_self_as_superuser(client, app_config):
    """ An superuser user should be able to retrieve it's own group information. """
    response = client.get(url_for('api_v1.group', id=1), headers=basic_auth_authorization_header('superuser', 'superuser'))
    assert response.status_code == 200

    data = response.get_json()

    # Check if create_time is in the response
    assert 'create_time' in data
    # get the create_time out if the response
    create_time = data.pop('create_time')

    # Check if time is a valid time format
    assert re.match(TIMESTAMP_REGEX, create_time) is not None

    # Test if everything is there expect for create_time
    assert data == {
        "groupname": "neuroradiologists",
        "uri": "/api/v1/groups/1",
        "name": "Neuroradiologists",
        "tasks": "/api/v1/groups/1/tasks",
        "users": "/api/v1/groups/1/users"
      }

def test_group_get_other_as_superuser(client, app_config):
    """ An superuser should be able to get any group information. """
    response = client.get(url_for('api_v1.group', id=1), headers=basic_auth_authorization_header('superuser', 'superuser'))
    assert response.status_code == 200

    data = response.get_json()

    # Check if create_time is in the response
    assert 'create_time' in data
    # get the create_time out if the response
    create_time = data.pop('create_time')

    # Check if time is a valid time format
    assert re.match(TIMESTAMP_REGEX, create_time) is not None

    # Test if everything is there expect for create_time
    assert data == {
        "groupname": "neuroradiologists",
        "uri": "/api/v1/groups/1",
        "name": "Neuroradiologists",
        "tasks": "/api/v1/groups/1/tasks",
        "users": "/api/v1/groups/1/users"
      }


def test_group_get_nonexisting_as_superuser(client, app_config):
    """ An superuser user should get a 404 (not found) when asking for a non existing group. """
    response = client.get(url_for('api_v1.group', id=10000), headers=basic_auth_authorization_header('superuser', 'superuser'))
    assert response.status_code == 404


    
def test_group_get_self_as_user(client, app_config):
    """ A normal user should be able to retrieve it's own group information. """
    response = client.get(url_for('api_v1.group', id=2), headers=basic_auth_authorization_header('user1', 'user1'))
    assert response.status_code == 200

    data = response.get_json()

    # Check if create_time is in the response
    assert 'create_time' in data
    # get the create_time out if the response
    create_time = data.pop('create_time')

    # Check if time is a valid time format
    assert re.match(TIMESTAMP_REGEX, create_time) is not None

    # Test if everything is there expect for create_time
    assert data == {
        "groupname": "students",
        "uri": "/api/v1/groups/2",
        "name": "Students",
        "tasks": "/api/v1/groups/2/tasks",
        "users": "/api/v1/groups/2/users"
      }


def test_group_get_other_as_user(client, app_config):
    """ A normal user should not be able to get information from another group. """
    response = client.get(url_for('api_v1.group', id=3), headers=basic_auth_authorization_header('user1', 'user1'))
    assert response.status_code == 403


def test_groups_get_not_authenticated(client, app_config, config):
    """ An anonymous user should be getting a 401 Unauthorized and a WWW-Authenticate header in the response. """
    response = client.get(url_for('api_v1.groups'), headers={'accept': 'application/json'})
    assert response.status_code == 401

    assert 'WWW-Authenticate' in response.headers
    assert f'Basic realm="{config["SECURITY_DEFAULT_HTTP_AUTH_REALM"]}"' == response.headers['WWW-Authenticate']


def test_group_get_not_authenticated(client, app_config, config):
    """ An anonymous user should be getting a 401 Unauthorized and a WWW-Authenticate header in the response. """
    response = client.get(url_for('api_v1.group', id=1), headers={'accept': 'application/json'})
    assert response.status_code == 401

    assert 'WWW-Authenticate' in response.headers
    # TODO: get the realm from the security config
    assert f'Basic realm="{config["SECURITY_DEFAULT_HTTP_AUTH_REALM"]}"' == response.headers['WWW-Authenticate']


def test_groups_post_as_admin(client, app_config):
    """ Adding a group as an admin should be allowed. """
    body = {
        "groupname": "test_group",
        "name": "Test Group"
    }
    response = client.post(url_for('api_v1.groups'),
                           json=body,
                           headers=basic_auth_authorization_header('admin', 'admin'))
    assert response.status_code == 201

    data = response.get_json()
    # Checks if all data is correct
    assert data['groupname'] == body['groupname']
    assert data['name'] == body['name']
    assert re.match(TIMESTAMP_REGEX, data['create_time']) is not None

    # Check if the uri's respond with 200 on a GET request.
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    uri_response = client.get(data['uri'], headers=headers)
    assert uri_response.status_code == 200
    #TODO uri's to tasks and users cannot be get!
    #tasks_response = client.get(data['tasks'], headers=headers)
    #assert tasks_response.status_code == 200
    #groups_response = client.get(data['users'], headers=headers)
    #assert groups_response.status_code == 200


def test_groups_post_as_superuser(client, app_config, config):
    """ Adding a group as an superuser should NOT be allowed. """
    body = {
        "groupname": "test_group",
        "name": "Test Group",
    }
    response = client.post(url_for('api_v1.groups'),
                           json=body,
                           headers=basic_auth_authorization_header('superuser', 'superuser'))
    assert response.status_code == 403

    data = response.get_json()
    assert data["response"]["error"] == config['SECURITY_MSG_UNAUTHORIZED'][0]


def test_groups_post_as_user(client, app_config, config):
    """ Adding a group as an user should NOT be allowed. """
    body = {
        "groupname": "test_group",
        "name": "Test Group",
    }
    response = client.post(url_for('api_v1.groups'),
                           json=body,
                           headers=basic_auth_authorization_header('user1', 'user1'))
    assert response.status_code == 403

    data = response.get_json()
    assert data["response"]["error"] == config['SECURITY_MSG_UNAUTHORIZED'][0]


def test_groups_post_as_anonymous(client, app_config, config):
    """ Adding a group as an anonymous user should NOT be allowed. """
    body = {
        "groupname": "test_group",
        "name": "Test Group",
    }
    response = client.post(url_for('api_v1.groups'), json=body, headers={'accept': 'application/json'})
    assert response.status_code == 401

    data = response.get_json()
    assert data["response"]["error"] == config['SECURITY_MSG_UNAUTHENTICATED'][0]


def test_groups_post_as_admin_missing_required_argument(client, app_config):
    """ One of the required arguments for adding a group is missing, this should return something went wrong [400]. """
    body = {
        "name": "Test Group",
    }
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.post(url_for('api_v1.groups'), json=body, headers=headers)
    assert response.status_code == 400

    data = response.get_json()
    print(data)
    # Check if the error message is about the required argument that was malformed.
    assert "Missing required parameter" in data["errors"]["groupname"]


def test_groups_post_as_admin_missing_optional_argument(client, app_config):
    """ One of the optional arguments for adding a new group is missing, this should create the group as expected with the default value for the missing parameter. """
    groupname = "test_group_optional"
    body = {
        "groupname": groupname,
    }
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.post(url_for('api_v1.groups'), json=body, headers=headers)
    print(f"response.get_json = {response.get_json()}")
    assert response.status_code == 201

    data = response.get_json()
    print(data)
    # Check if the name has gotten the default groupname value.
    assert data['name'] == groupname


def test_group_put_as_admin(client, app_config):
    """ Test if the admin can put a different name on the groups endpoint. This should be allowed. """
    body = {
        "name": "Different Name",
    }
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.put(url_for('api_v1.group', id=3), json=body, headers=headers)
    assert response.status_code == 200

    data = response.get_json()
    print(data)
    # Check if the data is correct in the response.
    assert data["name"] == body["name"]


def test_group_put_more_as_admin(client, app_config):
    """ Test if the admin can put more fields on the groups endpoint. This should be allowed. """
    body = {
        "groupname": "other_group_name",
        "name": "Another Name"
    }
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.put(url_for('api_v1.group', id=2), json=body, headers=headers)
    assert response.status_code == 200

    data = response.get_json()
    # Check if the data is correct in the response.
    body.pop('groupname')
    for key in body.keys():
        assert data[key] == body[key]
    body.pop('name')
    for key in body.keys():
        assert data[key] == body[key]
    #TODO are there more datafields that can be set in group?


def test_group_put_as_admin_non_existing_user(client, app_config):
    """ Test if the admin can put a different name on the groups endpoint of a non existing user. This should fail. """
    body = {
        "name": "Changed Name",
    }
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    response = client.put(url_for('api_v1.group', id=10), json=body, headers=headers)
    assert response.status_code == 404


def test_group_put_as_superuser_fields(client, app_config):
    """ Test if the superuser can put different fields on the groups endpoint. This should NOT be allowed. """
    fields = [
        ('groupname', 'some_other_name'),
        ('name', 'Changed Name'),
    ]
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    for fieldname, value in fields:
        print(f"{fieldname}: {value}")
        response = client.put(url_for('api_v1.group', id=1), json={fieldname: value}, headers=headers)
        assert response.status_code == 200

        data = response.get_json()
        # Check if the error message corresponds to what we expect
        assert data[fieldname] == value


def test_group_put_as_user_fields(client, app_config, config):
    """ Test if a user can put different fields on the groups endpoint. This should NOT be allowed. """
    fields = [
        ('groupname', 'some_other_name', 'You are not authorized to change the username'),
        ('name', 'Changed Name', 'You are not authorized to change the name'),
    ]
    headers = basic_auth_authorization_header('user2', 'user2')
    headers.update({'accept': 'application/json'})
    for fieldname, value, error_message in fields:
        print(f"{fieldname}: {value}")
        response = client.put(url_for('api_v1.group', id=2), json={fieldname: value}, headers=headers)
        assert response.status_code == 403

        data = response.get_json()
        # Check if the error message corresponds to what we expect.
        assert data["response"]["error"] == config['SECURITY_MSG_UNAUTHORIZED'][0]


def test_group_put_as_anonymous_name(client, app_config, config):
    """ Test if a anonymous user can put a different name on the group endpoint. This should NOT be allowed. """
    body = {
        "name": "Another Name",
    }
    headers = {'accept': 'application/json'}
    response = client.put(url_for('api_v1.group', id=3), json=body, headers=headers)
    print(response.get_json())
    assert response.status_code == 401

    data = response.get_json()
    print(data)
    # Check if the error message corresponds to what we expect.
    assert data["response"]["error"] == config['SECURITY_MSG_UNAUTHENTICATED'][0]
