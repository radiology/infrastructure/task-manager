# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pathlib

from flask import url_for

from taskmanager.tests.helpers import basic_auth_authorization_header
from taskmanager import models


def test_config_post(app, admin_config):
    client = app.test_client()
    config_filepath = pathlib.Path(__file__).parent.parent.parent.parent / 'tests' / 'config' / 'test_config.json'
    with open(config_filepath) as config_file:
        config_definition = config_file.read()

    response = client.post(url_for('api_v1.config'), headers=basic_auth_authorization_header("admin", "admin"),
                           json={'config_definition': config_definition})
    assert response.status_code == 201
    data = response.get_json()
    assert data.get('updated_at') is not None

    assert data['roles'] == [
        {'id': 2, 'name': 'superuser', 'uri': '/api/v1/roles/2'},
        {'id': 3, 'name': 'user', 'uri': '/api/v1/roles/3'},
    ]

    assert data['users'] == [
        {'id': 2, 'username': 'superuser', 'name': 'Super User', 'uri': '/api/v1/users/2',
         "assignment_weight": 1.0, "email": "superuser@blaat.nl"},
        {'id': 3, 'username': 'user1', 'name': 'User 1', 'uri': '/api/v1/users/3',
         "assignment_weight": 1.0, "email": "user1@blaat.nl"},
        {'id': 4, 'username': 'user2', 'name': 'User 2', 'uri': '/api/v1/users/4',
         "assignment_weight": 1.0, "email": "user2@blaat.nl"},
        {'id': 5, 'username': 'halfuser1', 'name': 'Half User 1', 'uri': '/api/v1/users/5',
         "assignment_weight": 0.5, "email": "halfuser1@blaat.nl"},
        {'id': 6, 'username': 'halfuser2', 'name': 'Half User 2', 'uri': '/api/v1/users/6',
         "assignment_weight": 0.5, "email": "halfuser2@blaat.nl"},
        {'id': 7, 'username': 'inactiveuser', 'name': 'Inactive User', 'uri': '/api/v1/users/7',
         "assignment_weight": 0.5, "email": "inactive@blaat.nl"}
    ]

    assert data['groups'] == [
        {'name': 'Neuroradiologists', 'groupname': 'neuroradiologists'},
        {'name': 'Students', 'groupname': 'students'},
        {'name': 'Inspectors', 'groupname': 'inspectors'}
    ]

    assert data['tags'] == [
        'Testing',
        'Quality Rating',
        'Something really important'
    ]


def test_forbidden_config_post(app, app_config):
    client = app.test_client()
    config_filepath = pathlib.Path(__file__).parent.parent.parent.parent / 'tests' / 'config' / 'test_config.json'
    with open(config_filepath) as config_file:
        config_definition = config_file.read()

    response = client.post(url_for('api_v1.config'), headers=basic_auth_authorization_header("user1", "user1"),
                           json={'config_definition': config_definition})
    assert response.status_code == 403
    response = client.post(url_for('api_v1.config'), headers=basic_auth_authorization_header("superuser", "superuser"),
                           json={'config_definition': config_definition})
    assert response.status_code == 403


def test_forbidden_config_put(app, app_config):
    client = app.test_client()
    config_filepath = pathlib.Path(__file__).parent.parent.parent.parent / 'tests' / 'config' / 'test_put_config.json'
    with open(config_filepath) as config_file:
        config_definition = config_file.read()

    response = client.put(url_for('api_v1.config'), headers=basic_auth_authorization_header("user1", "user1"),
                           json={'config_definition': config_definition})
    assert response.status_code == 403
    response = client.put(url_for('api_v1.config'), headers=basic_auth_authorization_header("superuser", "superuser"),
                           json={'config_definition': config_definition})
    assert response.status_code == 403


def test_config_put(app, app_config):
    user = models.User.query.filter_by(username="user2").one()
    assert user.username == "user2"
    assert user.email == "user2@blaat.nl"
    assert user.name == "User 2"

    group = models.Group.query.filter_by(groupname="neuroradiologists").one()
    assert [x.username for x in group.users] == ["user1", "halfuser2"]

    role = models.Role.query.filter_by(name='user').one()
    assert role.name == 'user'
    assert role.permissions == 'task_read,task_update_status,user_read,group_read'

    client = app.test_client()
    config_filepath = pathlib.Path(__file__).parent.parent.parent.parent / 'tests' / 'config' / 'test_put_config.json'
    with open(config_filepath) as config_file:
        config_definition = config_file.read()

    response = client.put('/api/v1/service/config/import-config',
                          headers=basic_auth_authorization_header("user1", "user1"),
                          json={'config_definition': config_definition})
    assert response.status_code == 403
    response = client.put('/api/v1/service/config/import-config',
                          headers=basic_auth_authorization_header("superuser", "superuser"),
                          json={'config_definition': config_definition})
    assert response.status_code == 403

    response = client.put('/api/v1/service/config/import-config',
                          headers=basic_auth_authorization_header("admin", "admin"),
                          json={'config_definition': config_definition})
    assert response.status_code == 200

    data = response.get_json()
    assert data.get('updated_at') is not None

    # Only updated objects have been changed
    assert data['users'] == [
        {'id': 4, 'username': 'user2', 'name': 'New User 2', 'uri': '/api/v1/users/4',
         "assignment_weight": 1.0, "email": "user2@blaat.nl"}
    ]
    assert data['roles'] == [{'id': 3, 'name': 'user', 'uri': '/api/v1/roles/3'}]
    assert data['tags'] == ['New tag']
    assert data['groups'] == [
        {"name": "Neuroradiologists", "groupname": "neuroradiologists"}
    ]

    # Check if no objects were added and see if the changes took effect.
    roles_json = client.get('/api/v1/roles', headers=basic_auth_authorization_header("admin", "admin")).get_json()
    assert roles_json['roles'] == [
        '/api/v1/roles/1',
        '/api/v1/roles/2',
        '/api/v1/roles/3'
    ]
    role = models.Role.query.filter_by(name='user').one()
    assert role.name == 'user'
    assert role.permissions == 'task_read,task_update_status,user_read,group_read,tag_add'

    user = models.User.query.filter_by(username="user2").one()
    assert user.username == "user2"
    assert user.email == "user2@blaat.nl"
    assert user.name == "New User 2"

    groups_json = client.get('/api/v1/groups', headers=basic_auth_authorization_header("admin", "admin")).get_json()
    assert groups_json['groups'] == [
        '/api/v1/groups/1',
        '/api/v1/groups/2',
        '/api/v1/groups/3'
    ]
    group = models.Group.query.filter_by(groupname="neuroradiologists").one()
    assert sorted([x.username for x in group.users]) == sorted(["user1", "halfuser2", "user2"])

    tags_json = client.get('/api/v1/tags', headers=basic_auth_authorization_header("admin", "admin")).get_json()
    assert tags_json['tags'] == [
        {'name': 'Testing', 'uri': '/api/v1/tags/1'},
        {'name': 'Quality Rating', 'uri': '/api/v1/tags/2'},
        {'name': 'Something really important', 'uri': '/api/v1/tags/3'},
        {'name': 'New tag', 'uri': '/api/v1/tags/4'}
    ]

