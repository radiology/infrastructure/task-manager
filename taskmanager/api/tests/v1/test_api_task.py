# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import re
from flask import url_for
from taskmanager.tests.helpers import basic_auth_authorization_header

TIMESTAMP_REGEX = r'\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d'


def test_tasks_get_as_admin(client, random_test_data):
    """ An admin user should be able to get the list of tasks. """
    response = client.get(url_for('api_v1.tasks'), headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list)


def test_tasks_get_as_superuser(client, random_test_data):
    """ A superuser should be able to get the list of tasks. """
    response = client.get(url_for('api_v1.tasks'), headers=basic_auth_authorization_header("superuser", "superuser"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list)


def test_tasks_get_as_user(client, random_test_data):
    """ A user should only be able to get the tasks assigned to him/her, not the whole list. """
    response = client.get(url_for('api_v1.tasks'), headers=basic_auth_authorization_header("user1", "user1"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    
    #TODO a user should only be able to see his own tasks or the tasks of the group he is in
    # doesnt seem to work yet
    response = client.get(url_for('api_v1.usertasks', id=3), headers=basic_auth_authorization_header("user1", "user1"))
    assert response.status_code == 200
    data = response.get_json()

def test_usertasks_get_as_admin(client, random_test_data):
    """ An admin user should be able to get the list of tasks. """
    response = client.get(url_for('api_v1.usertasks', id=3), headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list)


def test_usertasks_get_as_superuser(client, random_test_data):
    """ A superuser should be able to get the list of tasks. """
    response = client.get(url_for('api_v1.usertasks', id=5), headers=basic_auth_authorization_header("superuser", "superuser"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list)


def test_usertasks_get_as_user(client, random_test_data):
    """ A user should only be able to get the tasks assigned to him/her, not the whole list. """
    response = client.get(url_for('api_v1.usertasks',id=3), headers=basic_auth_authorization_header("user2", "user2"))
    assert response.status_code == 403

    #TODO a user should only be able to see his own tasks or the tasks of the group he is in
    # doesnt seem to work yet or ????

def test_task_get_as_admin(client, random_test_data):
    """ An admin should be able to get the tasks assigned to another user. """
    response = client.get(url_for('api_v1.usertasks', id=1), headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200


def test_task_get_as_superuser(client, random_test_data):
    """ A superuser should be able to get the tasks assigned to another user. """
    response = client.get(url_for('api_v1.usertasks', id=2), headers=basic_auth_authorization_header("superuser", "superuser"))
    assert response.status_code == 200


def test_task_get_as_user(client, random_test_data):
    """ A user should be able to get the tasks assigned himself. """
    response = client.get(url_for('api_v1.usertasks', id=3), headers=basic_auth_authorization_header("user1", "user1"))
    assert response.status_code == 200


def test_task_get_nonexisting_as_admin(client, random_test_data):
    """ An admin should not be able to get a nonexisting task. """
    response = client.get(url_for('api_v1.usertasks', id=10000), headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 404


def test_task_get_nonexisting_as_superuser(client, random_test_data):
    """ A superuser should not be able to get a nonexisting task. """
    response = client.get(url_for('api_v1.usertasks', id=20000), headers=basic_auth_authorization_header("superuser", "superuser"))
    assert response.status_code == 404


def test_task_get_nonexisting_as_user(client, random_test_data):
    """ A user should be not able to get a nonexisting task. """
    response = client.get(url_for('api_v1.usertasks', id=30000), headers=basic_auth_authorization_header("user1", "user1"))
    assert response.status_code == 404


def test_task_get_nonexisting_as_other_user(client, random_test_data):
    """ A user should not be able to get a nonexisting task. """
    response = client.get(url_for('api_v1.usertasks', id=40000), headers=basic_auth_authorization_header("user2", "user2"))
    assert response.status_code == 404

def test_tasks_userfilter_get_as_admin(client, random_test_data):
    """ An admin should be able to get the tasks filtered on a specific user. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?user=user1", headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)
    #TODO How is the userfilter working. Looks like it is ignored if it is forbidden. In that case it gets your own tasks?


def test_tasks_nonexisting_userfilter_get_as_admin(client, random_test_data):
    """ An admin should be able to get the tasks filtered on a specific user. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?user=user5", headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 404


def test_tasks_userfilter_get_as_superuser(client, random_test_data):
    """ A superuser should be able to get the tasks filtered on a specific user. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?user=user1", headers=basic_auth_authorization_header("superuser", "superuser"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)


def test_tasks_userfilter_get_as_user(client, random_test_data):
    """ A user should only be able to get the tasks filtered on a his own. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?user=user1", headers=basic_auth_authorization_header("user1", "user1"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)


def test_tasks_userfilter_get_as_otheruser(client, random_test_data):
    """ An user should not be able to get the tasks filtered on another user. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?user=user1", headers=basic_auth_authorization_header("user2", "user2"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)


def test_tasks_templatefilter_get_as_admin(client, random_test_data):
    """ An admin should be able to get the tasks filtered on a specific template. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?template=student", headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)

def test_tasks_nonexisting_templatefilter_get_as_admin(client, random_test_data):
    """ An admin should not be able to get the tasks filtered on a nonexisting template. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?template=professor", headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 404


def test_tasks_templatefilter_get_as_superuser(client, random_test_data):
    """ A superuser should be able to get the tasks filtered on a specific template. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?template=neuroradiologist", headers=basic_auth_authorization_header("superuser", "superuser"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)


def test_tasks_templatefilter_get_as_user(client, random_test_data):
    """ A user should only be able to get the tasks filtered on a template if the tasks belong to him. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?template=microbleeds", headers=basic_auth_authorization_header("user1", "user1"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)


def test_tasks_templatefilter_get_as_otheruser(client, random_test_data):
    """ An user should not be able to get the tasks filtered on a template that belong to another user. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?template=student", headers=basic_auth_authorization_header("user2", "user2"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)
    #TODO How does the filtering works for users?


def test_tasks_tagfilter_get_as_admin(client, random_test_data):
    """ An admin should be able to get the tasks filtered on a specific tag. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?tag=Testing", headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)

def test_tasks_nonexisting_tagfilter_get_as_admin(client, random_test_data):
    """ An admin should not be able to get the tasks filtered on a nonexisting tag. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?tag=biking", headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)
    #TODO .... How does it work if you filter on a nonexisting tag?


def test_tasks_tagfilter_get_as_superuser(client, random_test_data):
    """ A superuser should be able to get the tasks filtered on a specific tag. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?tag=Testing", headers=basic_auth_authorization_header("superuser", "superuser"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)


def test_tasks_tagfilter_get_as_user(client, random_test_data):
    """ A user should only be able to get the tasks filtered on a specific tag if the tasks are assigned to him. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?tag=Testing", headers=basic_auth_authorization_header("user1", "user1"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)


def test_tasks_tagfilter_get_as_otheruser(client, random_test_data):
    """ An user should not be able to get the tasks filtered on a specific tag if the tasks do not belong to him. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?tag=Testing", headers=basic_auth_authorization_header("user2", "user2"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)


def test_tasks_projfilter_get_as_admin(client, random_test_data):
    """ An admin should be able to get the tasks filtered on a specific template. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?project=test", headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)

def test_tasks_nonexisting_projfilter_get_as_admin(client, random_test_data):
    """ An admin should not be able to get the tasks filtered on a nonexisting template. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?project=aetionomy", headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)


def test_tasks_projfilter_get_as_superuser(client, random_test_data):
    """ A superuser should be able to get the tasks filtered on a specific template. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?project=test", headers=basic_auth_authorization_header("superuser", "superuser"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)


def test_tasks_projfilter_get_as_user(client, random_test_data):
    """ A user should only be able to get the tasks filtered on a template if the tasks belong to him. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?project=test", headers=basic_auth_authorization_header("user1", "user1"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)


def test_tasks_projfilter_get_as_otheruser(client, random_test_data):
    """ An user should not be able to get the tasks filtered on a template that belong to another user. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?project=test", headers=basic_auth_authorization_header("user2", "user2"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)
    #TODO How does the filtering works for users?


def test_tasks_appfilter_get_as_admin(client, random_test_data):
    """ An admin should be able to get the tasks filtered on a specific template. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?application_name=TaskR", headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)

def test_tasks_nonexisting_appfilter_get_as_admin(client, random_test_data):
    """ An admin should not be able to get the tasks filtered on a nonexisting template. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?application_name=Facebook", headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200
    # Filtered on not existing gives an empty list

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)


def test_tasks_appfilter_get_as_superuser(client, random_test_data):
    """ A superuser should be able to get the tasks filtered on a specific template. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?application_name=TaskR", headers=basic_auth_authorization_header("superuser", "superuser"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)


def test_tasks_appfilter_get_as_user(client, random_test_data):
    """ A user should only be able to get the tasks filtered on a template if the tasks belong to him. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?application_name=TaskR", headers=basic_auth_authorization_header("user1", "user1"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)


def test_tasks_appfilter_get_as_otheruser(client, random_test_data):
    """ An user should not be able to get the tasks filtered on a template that belong to another user. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?application_name=TaskR", headers=basic_auth_authorization_header("user2", "user2"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)
    #TODO How does the filtering works for users?


def test_tasks_statusfilter_get_as_admin(client, random_test_data):
    """ An admin should be able to get the tasks filtered on a specific template. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?status=queued", headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)

def test_tasks_nonexisting_statusfilter_get_as_admin(client, random_test_data):
    """ An admin should not be able to get the tasks filtered on a nonexisting template. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?status=shopping", headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)


def test_tasks_statusfilter_get_as_superuser(client, random_test_data):
    """ A superuser should be able to get the tasks filtered on a specific template. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?status=queued", headers=basic_auth_authorization_header("superuser", "superuser"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)


def test_tasks_statusfilter_get_as_user(client, random_test_data):
    """ A user should only be able to get the tasks filtered on a template if the tasks belong to him. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?status=queued", headers=basic_auth_authorization_header("user1", "user1"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)


def test_tasks_statusfilter_get_as_otheruser(client, all_users_data):
    """ An user should not be able to get the tasks filtered on a template that belong to another user. """
    response = client.get("http://127.0.0.1:5000/api/v1/tasks?status=queued", headers=basic_auth_authorization_header("user2", "user2"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'tasks' in data
    assert isinstance(data['tasks'], list) 
    print(data)
    #TODO How does the filtering works for users?


def test_task_delete_as_admin(client, all_users_data):
    """ An admin user should be able to delete a task. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    
    response = client.get(url_for('api_v1.tasks'), headers=headers)
    task_id = response.get_json()['tasks'][-1]['id']
    
    response = client.delete(url_for('api_v1.task', id=task_id), headers=headers)
    assert response.status_code == 200


def test_task_delete_superuser(client, all_users_data):
    """ A superuser user should not be able to delete a task. """
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    
    response = client.get(url_for('api_v1.tasks'), headers=headers)
    task_id = response.get_json()['tasks'][0]['id']

    response = client.delete(url_for('api_v1.task', id=task_id), headers=headers)
    assert response.status_code == 403


def test_task_delete_as_user(client, random_test_data):
    """ A user user should not be able to delete a task. """
    headers = basic_auth_authorization_header('user2', 'user2')
    headers.update({'accept': 'application/json'})
    
    response = client.delete(url_for('api_v1.task_template', id=3), headers=headers)
    assert response.status_code == 403


def test_task_delete_nonexisting_as_admin(client, random_test_data):
    """ An admin user should not be able to delete a nonexisting task. """
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    
    response = client.delete(url_for('api_v1.task_template', id=10000), headers=headers)
    assert response.status_code == 404


def test_task_put_as_admin(client, all_users_one_task_data):
    """ An admin user should be able to put anything to a task. """

    data = [
        ('project', 'new_admin_project', 'new_admin_project', 200),
        ('template', 'student', '/api/v1/task_templates/2', 200),
        ('content', 'new_admin_content', 'new_admin_content', 200),
        ('status', 'new_admin_status', 'new_admin_status', 200),
        ('callback_url', 'https://new/admin/url', 'https://new/admin/url', 200),
        ('callback_content', 'new_admin_callback_content', 'new_admin_callback_content', 200),
        ('generator_url', 'https://new/admin/generator/url', 'https://new/admin/generator/url', 200),
        ('application_name', 'new_admin_application_name', 'new_admin_application_name', 200),
        ('application_version', '1.2.3.4_new', '1.2.3.4_new', 200),
        ('tags', ['Quality Rating'], ['/api/v1/tags/2'], 200),
        ('users', [3], ['/api/v1/users/3'], 200),
        ('groups', [2], ['/api/v1/groups/2'], 200)
    ]

    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})

    # Get a task from superuser
    response = client.get(url_for('api_v1.usertasks', id=2), headers=headers)
    task_id = response.get_json()['tasks'][0]['id']

    response = client.get(url_for('api_v1.tags'), headers=headers)
    
    for field, value, expected_response_value, expected_response in data:
        response = client.put(url_for('api_v1.task', id=task_id), json={field: value}, headers=headers)
        assert response.status_code == expected_response

        response = client.get(url_for('api_v1.task', id=task_id), headers=headers)
        data = response.get_json()
        assert data[field] == expected_response_value


def test_task_put_as_superuser(client, all_users_one_task_data):
    """ A superuser user should be able to put status, users and groups to a task. Nothing else. """
    data = [
        ('project', 'new_admin_project', 'new_admin_project', 403),
        ('template', 'student', '/api/v1/task_templates/2', 403),
        ('content', 'new_admin_content', 'new_admin_content', 403),
        ('status', 'new_admin_status', 'new_admin_status', 200),
        ('callback_url', 'https://new/admin/url', 'https://new/admin/url', 403),
        ('callback_content', 'new_admin_callback_content', 'new_admin_callback_content', 403),
        ('generator_url', 'https://new/admin/generator/url', 'https://new/admin/generator/url', 403),
        ('application_name', 'new_superuser_application_name', 'new_superuser_application_name', 403),
        ('application_version', '1.2.3.5_new', '1.2.3.5_new', 403),
        ('tags', ["Quality Rating"], ['/api/v1/tags/2'], 403),
        ('users', [3], ['/api/v1/users/3'], 200),
        ('groups', [2], ['/api/v1/groups/2'], 200)
    ]
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})

    # Get a task from user1
    response = client.get(url_for('api_v1.usertasks', id=3), headers=headers)
    task_id = response.get_json()['tasks'][0]['id']
    
    for field, value, expected_response_value, expected_response in data:
        response = client.put(url_for('api_v1.task', id=task_id), json={field: value}, headers=headers)
        assert response.status_code == expected_response

        if expected_response == 200:
            response = client.get(url_for('api_v1.task', id=task_id), headers=headers)
            data = response.get_json()
            assert data[field] == expected_response_value


def test_task_put_as_user(client, all_users_one_task_data):
    """ A user user should be able to put the status of a task, nothing else. """
    data = [
        ('project', 'new_admin_project', 'new_admin_project', 403),
        ('template', 'student', '/api/v1/task_templates/2', 403),
        ('content', 'new_admin_content', 'new_admin_content', 403),
        ('status', 'new_admin_status', 'new_admin_status', 200),
        ('callback_url', 'https://new/admin/url', 'https://new/admin/url', 403),
        ('callback_content', 'new_admin_callback_content', 'new_admin_callback_content', 403),
        ('generator_url', 'https://new/admin/generator/url', 'https://new/admin/generator/url', 403),
        ('application_name', 'new_user_application_name', 'new_user_application_name', 403),
        ('application_version', '1.2.3.6_new', '1.2.3.6_new', 403),
        ('tags', ["Quality Rating"], ['/api/v1/tags/2'], 403),
        ('users', [3], ['/api/v1/users/3'], 403),
        ('groups', [2], ['/api/v1/groups/2'], 403)
    ]
    headers = basic_auth_authorization_header('user1', 'user1')
    headers.update({'accept': 'application/json'})

    response = client.get(url_for('api_v1.usertasks', id=3), headers=headers)
    task_id = response.get_json()['tasks'][0]['id']
    
    for field, value, expected_response_value, expected_response in data:
        response = client.put(url_for('api_v1.task', id=task_id), json={field: value}, headers=headers)
        assert response.status_code == expected_response

        if expected_response == 200:
            response = client.get(url_for('api_v1.task', id=task_id), headers=headers)
            data = response.get_json()
            assert data[field] == expected_response_value

    
def test_task_put_nonexisting_as_admin(client, random_test_data):
    """ An admin should not be able to put a nonexisting task. """
    body = {"project": "mynewproject"}
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    
    response = client.put(url_for('api_v1.task', id=400000), json=body, headers=headers)
    assert response.status_code == 404


def test_tasks_post_as_admin(client, app_config):
    """ An admin user should be able to post tasks. """
    body = {
        "project": "newproject",
        "template": "student",
        "content": "some random content",
        "tags": ["Quality Rating"],
        "callback_url": "https://some/url",
        "callback_content": "some json",
        "generator_url": "https://another/url",
        "application_name": "TaskManager",
        "application_version": "1.0.0",
        "users": [3],
        "groups": [2]
    }
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    
    response = client.post(url_for('api_v1.tasks'), json=body, headers=headers)
    assert response.status_code == 201


def test_tasks_post_superuser(client, app_config):
    """ A superuser should not be able to post tasks. """
    body = {
        "project": "newproject",
        "template": "student",
        "content": "some random content",
        "tags": [2],
        "callback_url": "https://some/url",
        "callback_content": "some json",
        "generator_url": "https://another/url",
        "application_name": "TaskManager",
        "application_version": "1.0.0",
        "users": [3],
        "groups": [2]
    }
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    
    response = client.post(url_for('api_v1.tasks'), json=body, headers=headers)
    assert response.status_code == 403


def test_tasks_post_as_user(client, app_config):
    """ A user should not be able to post a new task. """
    body = {
        "project": "newproject",
        "template": "student",
        "content": "some random content",
        "tags": [2],
        "callback_url": "https://some/url",
        "callback_content": "some json",
        "generator_url": "https://another/url",
        "application_name": "TaskManager",
        "application_version": "1.0.0",
        "users": [3],
        "groups": [2]
    }
    headers = basic_auth_authorization_header('user2', 'user2')
    headers.update({'accept': 'application/json'})
    
    response = client.post(url_for('api_v1.tasks'), json=body, headers=headers)
    assert response.status_code == 403

