# Copyright 2017-2021 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from flask import url_for
from taskmanager.tests.helpers import basic_auth_authorization_header

def test_meta_get_as_admin(client, meta_task):
    """ An admin user should be able to get the list of meta. """
    response = client.get(url_for('api_v1.meta'), headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 200

    data = response.get_json()
    assert 'meta' in data
    assert isinstance(data['meta'], list)

def test_meta_get_as_superuser(client, meta_task):
    """ A superuser user should not be able to get the list of meta. """
    response = client.get(url_for('api_v1.meta'), headers=basic_auth_authorization_header("superuser", "superuser"))
    assert response.status_code == 403

def test_meta_get_as_user(client, meta_task):
    """ A regular user should not be able to get the list of meta. """
    response = client.get(url_for('api_v1.meta'), headers=basic_auth_authorization_header("user1", "user1"))
    assert response.status_code == 403

def test_non_existing_meta_get_as_admin(client, meta_task):
    """ An admin user should not be able to get the list of meta with 404 code returned. """
    response = client.get(url_for('api_v1.meta_id', id=1234), headers=basic_auth_authorization_header("admin", "admin"))
    assert response.status_code == 404

def test_non_existing_meta_get_as_superuser(client, meta_task):
    """ A superuser user should not be able to get the list of meta with 403 code returned. """
    response = client.get(url_for('api_v1.meta_id', id=1234), headers=basic_auth_authorization_header("superuser", "superuser"))
    assert response.status_code == 403

def test_non_existing_meta_get_as_user(client, meta_task):
    """ A regular user should not be able to get the list of meta with 403 code returned. """
    response = client.get(url_for('api_v1.meta_id', id=1234), headers=basic_auth_authorization_header("user1", "user1"))
    assert response.status_code == 403

def test_meta_add_as_admin(client, meta_task):
    """ An admin user should be able to add new meta entries. """
    body = {
        "label": "XNAT",
        "value": "https://domain",
    }
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    
    response = client.post(url_for('api_v1.meta'), json=body, headers=headers)
    assert response.status_code == 201

def test_meta_add_as_superuser(client, meta_task):
    """ A superuser user should not be able to add the list of meta. """
    body = {
        "label": "XNAT",
        "value": "https://domain",
    }
    headers = basic_auth_authorization_header('superuser', 'superuser')
    headers.update({'accept': 'application/json'})
    
    response = client.post(url_for('api_v1.meta'), json=body, headers=headers)
    assert response.status_code == 403


def test_meta_add_as_user(client, meta_task):
    """ A regular user should not be able to get the list of meta. """
    body = {
        "label": "XNAT",
        "value": "https://domain",
    }
    headers = basic_auth_authorization_header('user1', 'user1')
    headers.update({'accept': 'application/json'})
    
    response = client.post(url_for('api_v1.meta'), json=body, headers=headers)
    assert response.status_code == 403

def test_new_meta(client, meta_task):
    response = client.get(url_for('api_v1.task', id=9999), headers=basic_auth_authorization_header("admin", "admin"))
    
    assert response.status_code == 200
    data = response.get_json()
    # Check that substitution has not happened yet
    assert ("$SYSTEM_URL" in data["content"]) and ("$SYSTEM_URL" in data["callback_url"])
    assert ("TESTDOMAIN" not in data["content"]) and ("TESTDOMAIN" not in data["callback_url"])

    # POST new Meta entry
    body = {
        "label": "SYSTEM_URL",
        "value": "https://TESTDOMAIN",
    }
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    
    response = client.post(url_for('api_v1.meta'), json=body, headers=headers)
    assert response.status_code == 201

    response = client.get(url_for('api_v1.task', id=9999), headers=basic_auth_authorization_header("admin", "admin"))
    
    assert response.status_code == 200
    data = response.get_json()
    # Check that substitution has happened
    assert ("$SYSTEM_URL" not in data["content"]) and ("$SYSTEM_URL" not in data["callback_url"])
    assert ("TESTDOMAIN" in data["content"]) and ("TESTDOMAIN" in data["callback_url"])

def test_meta_history(client, meta_task):
    
    # POST new Meta entry
    body = {
        "label": "STUDYGOV_DOMAIN",
        "value": "https://TESTDOMAIN",
    }
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})
    
    response = client.post(url_for('api_v1.meta'), json=body, headers=headers)
    assert response.status_code == 201

    response = client.get(url_for('api_v1.meta_id', id="STUDYGOV_DOMAIN"), headers=basic_auth_authorization_header("admin", "admin"))
    
    assert response.status_code == 200
    data = response.get_json()
    
    # Check that correct value has been stored
    assert "history" in data
    assert isinstance(data["history"], list)
    assert len(data["history"]) == 1
    assert data["history"][0]['value'] == "https://TESTDOMAIN"

    body = {
        "value": "https://NEWDOMAIN",
    }
    headers = basic_auth_authorization_header('admin', 'admin')
    headers.update({'accept': 'application/json'})

    # Update Meta entry value    
    response = client.put(url_for('api_v1.meta_id', id="STUDYGOV_DOMAIN"), json=body, headers=headers)
    assert response.status_code == 200

    response = client.get(url_for('api_v1.meta_id', id="STUDYGOV_DOMAIN"), headers=basic_auth_authorization_header("admin", "admin"))
    
    assert response.status_code == 200
    data = response.get_json()

    # Check that previous value has been stored
    assert "history" in data
    assert isinstance(data["history"], list)
    assert len(data["history"]) == 2
    assert data["history"][0]['value'] == "https://TESTDOMAIN"
    assert data["history"][1]['value'] == "https://NEWDOMAIN"
