import json
import string
import random

from .. import models
from ..control import insert_task


def create_locks(app):
    """ Create a lock on the first tasks of a user. """
    db = models.db
    with app.app_context():
        users = models.User.query.all()
        for u in users:
            u.tasks[0].lock = u
        db.session.commit()

def random_assign(population, k=None, seed=None):
    if seed is not None:
        random.seed(seed)
    if k is None:
        return random.sample(population=population, k=random.randint(1, len(population)))
    else:
        return random.sample(population=population, k=k)
 
def create_random_test_taskgroup(app, num_tasks=3, silent=False):
    db = models.db
    with app.app_context():
        tasks = models.Task.query.all()
        number_of_taskgroups = models.TaskGroup.query.count()
        random_tasks = random_assign(population=tasks, k=num_tasks)
        db.session.add(models.TaskGroup(
                label="TaskGroup {}".format(number_of_taskgroups + 1), 
                callback_url="http://localhost:5000/experiment/1",
                callback_content='{"function": "do_something_useful"}',
                tasks=random_tasks)
                )
        db.session.commit()
        if not silent:
            print(f"Test Taskgroup with {num_tasks} random tasks created!")


def create_test_tasks_all_users(app, base_tasks_path, num_tasks=5, silent=False):
    db = models.db
    with app.app_context():
        users = models.User.query.all()

        for base_task_file in base_tasks_path.iterdir():
            for user in users:
                load_test_tasks(base_task_file, users=[user], groups=None, tags=None, num_tasks=num_tasks)


def create_random_test_tasks(app, base_tasks_path, silent=False):
    db = models.db
    with app.app_context():
        users = models.User.query.all()
        groups = models.Group.query.all()
        tags = models.Tag.query.all()

        for base_task_file in base_tasks_path.iterdir():
            random_users = random_assign(population=users)
            random_groups = random_assign(population=groups)
            random_tags = random_assign(population=tags)
            # Either users or groups has to be None, so choose...
            if random_assign([True, False], k=1):
                random_users = None
            else:
                random_groups = None
            load_test_tasks(base_task_file, users=random_users, groups=random_groups, tags=random_tags, num_tasks=random.randint(1, 10))

        if not silent:
            print("Test Tasks tasks were created!")


def load_test_tasks(base_task, users=None, groups=None, tags=None, num_tasks=5, silent=False):
    db = models.db
    base_task_content = base_task.read_text()
    base_task_template = string.Template(base_task_content)
    template = json.loads(base_task_content)['template']
    for num in range(num_tasks):
        content = base_task_template.substitute({"EXPERIMENT_ID": f"experiment_{num:05d}", 
                                                 "SUBJECT_ID": "subject_00001", 
                                                 "SYSTEM_URL": "https://xnat.example.org"})
        insert_task(
            content=content,
            project="test",
            callback_url="http://localhost:5000/",
            callback_content="http://localhost:5000/",
            template=template,
            generator_url="https://localhost:5000/",
            tags=tags,
            users=users,
            groups=groups,
            distribute_in_group=None,
            application_name="TaskR",
            application_version="0.1.0",
            commit_to_db=False)

    # Commit everything after the inserts.
    db.session.commit()
                    