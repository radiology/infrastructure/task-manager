# Copyright 2017 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import datetime
import pprint
from typing import Optional

from flask_security import SQLAlchemyUserDatastore, current_user
from flask_security.utils import hash_password

from .. import models
from .. import exceptions


class LogginMiddleware(object):
    def __init__(self, app):
        self._app = app

    def __call__(self, environ, resp):
        errorlog = environ['wsgi.errors']
        pprint.pprint(("REQUEST", environ), stream=errorlog)

        def log_response(status, headers, *args):
            pprint.pprint(("RESPONSE", status, headers), stream=errorlog)
            return resp(status, headers, *args)

        return self._app(environ, log_response)


def get_object_from_arg(id, model, model_name=None, skip_id=False, allow_none=False):
    data = None

    if isinstance(id, model):
        return id

    if id is not None:
        # If we have a URI/path we just want the last part
        if isinstance(id, str) and '/' in id:
            id = id.rsplit('/', 1)[1]

        # For id try to cast to an int
        if not skip_id:
            try:
                id = int(id)
            except (TypeError, ValueError) as e:
                pass

        if isinstance(id, int):
            data = model.query.filter(model.id == id).one_or_none()
        elif model_name is not None:
            data = model.query.filter(model_name == id).one_or_none()

    if data is None and not allow_none:
        raise exceptions.CouldNotFindResourceError(id, model)

    return data


def create_user(username: str,
                name: str,
                password: str,
                email: str,
                assignment_weight: Optional[float] = 1.0,
                active: bool = True,
                confirm: bool = True,
                force: bool = False):
    """Helper to create a user"""
    password = hash_password(password)
    confirmed_at = datetime.datetime.now() if confirm else None

    user_datastore = SQLAlchemyUserDatastore(models.db, models.User, models.Role)

    db_user = user_datastore.create_user(
        username=username,
        name=name,
        password=password,
        email=email,
        active=active,
        confirmed_at=confirmed_at,
        assignment_weight=assignment_weight,
    )

    doit = False
    if not force:
        doit = input(f"Are you sure you want to commit user [{db_user.username}], to database [yes/no]: ") == 'yes'

    if doit or force:
        models.db.session.commit()
        print("\n * Committed to the database.")
    else:
        models.db.session.rollback()
        print("\n * Cancelled.")

    return db_user


def json_type(data):
    """
    A simple type for request parser that just takes all json as is
    """
    return data


def int_or_str(value):
    return value if isinstance(value, int) else str(value)


def list_of_int_or_str(value):
    if not isinstance(value, list):
        value = list(value)
    return [int_or_str(x) for x in value]
    

def has_permission_any(*args):
    return any(current_user.has_permission(perm) for perm in args)


def has_permission_all(*args):
    return all(current_user.has_permission(perm) for perm in args)
