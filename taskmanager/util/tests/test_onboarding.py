import os
import pytest

from taskmanager.util.onboarding import ensure_roles
from taskmanager.util.onboarding import ensure_admin_user

from taskmanager.models import Role
from taskmanager.models import User


def test_ensure_roles(init_db):
    roles = Role.query.all()
    print(f"{roles=}")
    assert roles == []

    ensure_roles()
    roles = Role.query.order_by(Role.name).all()
    assert [x.name for x in roles] == ['admin', 'superuser', 'user']


def test_ensure_admin(mocker, init_db):
    users = User.query.all()
    print(f"{users=}")
    assert users == []

    test_envs = {
        'TASKMAN_ADMIN_USERNAME': "admin",
        'TASKMAN_ADMIN_EMAIL': "admin@localhost.local",
        'TASKMAN_ADMIN_PASSWORD': "blaat123"
    }
    
    mocker.patch.dict(os.environ, test_envs)
    ensure_roles()
    ensure_admin_user()
    users = User.query.order_by(User.username).all()
    assert len(users) == 1
    admin_user = users[0]
    assert admin_user.username == "admin"
    

def test_ensure_admin_error_noenvvars(capsys, init_db):
    users = User.query.all()
    print(f"{users=}")
    assert users == []

    ensure_roles()
    ensure_admin_user()

    users = User.query.all()
    print(f"{users=}")
    assert users == []

    captured = capsys.readouterr()
    assert "Can only ensure admin account if TASKMAN_ADMIN_USERNAME, TASKMAN_ADMIN_EMAIL, and TASKMAN_ADMIN_PASSWORD are set" in captured.out