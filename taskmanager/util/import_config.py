import os
import datetime

import yaml
from flask_security import SQLAlchemyUserDatastore
from flask_security.utils import hash_password

from .. import models, user_datastore


def load_roles(roles_config, overwrite, silent):
    updated_roles = list()
    for role in roles_config:
        role_query = models.Role.query.filter(models.Role.name == role['name'])
        existing_role = role_query.first()
        if existing_role is None:
            new_role = user_datastore.create_role(**role)
            updated_roles.append(new_role)
            if not silent:
                print(f"Created {new_role}")
        elif overwrite:
            if role.get('permissions'):
                role['permissions'] = ",".join(role['permissions'])
            role_query.update(role)
            updated_roles.append(existing_role)
            if not silent:
                print(f"Updated {existing_role}")
        else:
            print(f"[WARNING] Skipping Role {role['name']}, already exists!")
    return updated_roles


def load_users(users_config, overwrite, silent):
    updated_users = list()
    for user in users_config:
        user_query = models.User.query.filter((models.User.username == user['username'])
                                              | (models.User.email == user['email']))
        existing_user = user_query.first()
        if existing_user is None:
            user['confirmed_at'] = datetime.datetime.now()
            user['password'] = hash_password(user['password'])
            new_user = user_datastore.create_user(**user)
            updated_users.append(new_user)
            if not silent:
                print(f"Adding {new_user}")
        elif overwrite:
            user['confirmed_at'] = datetime.datetime.now()
            user['password'] = hash_password(user['password'])
            if user.get('roles'):
                user_roles = [user_datastore.find_role(role) for role in user['roles']]
                user.pop('roles')
                existing_user.roles = user_roles
            user_query.update(user)
            updated_users.append(existing_user)
            if not silent:
                print(f"Updated {existing_user}")
        else:
            if not silent:
                print(f"[WARNING] Skipping User {user['username']} / {user['email']}, "
                      + "user and/or username already used!")
    return updated_users


def load_groups(groups_config, overwrite, silent):
    db = models.db
    updated_groups = list()
    for group in groups_config:
        group_query = models.Group.query.filter((models.Group.groupname == group['groupname'])
                                                | (models.Group.name == group['name']))
        existing_group = group_query.first()
        if existing_group is None:
            db.session.add(models.Group(**group))
            updated_groups.append(group)
            if not silent:
                print(f"* {group}")
        elif overwrite:
            if group.get('members'):
                group_members = [user_datastore.find_user(username=x, case_insensitive=False) for x in group.pop('members')]
                existing_group.users = group_members
            group_query.update(group)
            updated_groups.append(group)
        else:
            print(f"[WARNING] Skipping Group {group['groupname']} already exists!")
    return updated_groups


def load_tags(tags_config, silent):
    db = models.db
    updated_tags = list()

    for tag in tags_config:
        if models.Tag.query.filter(models.Tag.name == tag).count() == 0:
            db.session.add(models.Tag(tag))
            updated_tags.append(tag)
            if not silent:
                print(f"* {tag}")
        else:
            print(f"[WARNING] Skipping Tag {tag}, already exists!")
    return updated_tags


def load_config(config, overwrite=False, silent=True):
    db = models.db

    updated_objects = {
        'roles': list(),
        'users': list(),
        'groups': list(),
        'tags': list()
    }

    if 'roles' in config:
        if not silent:
            print("\n Adding Roles:")
        updated_objects['roles'] = load_roles(config['roles'], overwrite, silent)

    if 'users' in config:
        if not silent:
            print("\nAdding users:")
        updated_objects['users'] = load_users(config['users'], overwrite, silent)

    if 'groups' in config:
        if not silent:
            print("\nAdding groups:")
        updated_objects['groups'] = load_groups(config['groups'], overwrite, silent)

    if 'tags' in config:
        if not silent:
            print("\nAdding Tags:")
        updated_objects['tags'] = load_tags(config['tags'], silent)

    db.session.commit()

    return updated_objects


def load_config_file(app, file_path, silent=False):
    file_path = str(file_path)

    with app.app_context():
        db = models.db

        if not os.path.isfile(file_path):
            raise ValueError(f"The file ({file_path}) does not exist")

        basedir = os.path.dirname(os.path.abspath(file_path))
        with open(file_path) as fh:
            config = yaml.safe_load(fh)

        load_config(config, silent=silent)

        if 'templates' in config:
            if not silent:
                print("\nAdding templates:")
            for template in config['templates']:
                template_path = os.path.normpath(os.path.join(basedir, template))

                if not os.path.isfile(template_path):
                    db.session.rollback()
                    raise ValueError(f"The file ({template_path}) does not exist")

                with open(template_path) as fh:
                    template_content = yaml.safe_load(fh)
                    template_name = template_content['template_name']
                    if models.TaskTemplate.query.filter(models.TaskTemplate.label == template_name).count() == 0:
                        db.session.add(models.TaskTemplate(template_name, yaml.safe_dump(template_content)))
                        if not silent:
                            print(f"* Adding [{template_name}] from {template_path}")
                    else:
                        print(f"[WARNING] Skipping Template {template_name}, already exists!")

        if not silent:
            print("\nCommitting to the database ...")
        db.session.commit()
        if not silent:
            print("[ DONE ]")
