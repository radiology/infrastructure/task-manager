import datetime
import os

from .. import models
from .. import user_datastore
from ..util.helpers import hash_password


ROLES_DEFINITION = [
      {"name": "admin",
       "permissions": ["task_read_all", 
                       "task_update_all",
                       "task_add",
                       "task_delete",
                       "template_add",
                       "template_update",
                       "template_delete",
                       "tag_add",
                       "tag_update",
                       "tag_delete",
                       "user_read_all",
                       "user_update_all",
                       "user_add",
                       "user_delete",
                       "group_read_all",
                       "group_add",
                       "group_update",
                       "group_delete",
                       "meta_read_all",
                       "meta_update",
                       "meta_add",
                       "roles_manage"],
       "description": "admin"},
      {"name": "superuser",
       "permissions": ["task_read_all",
                       "task_update_status_all",
                       "task_update_user",
                       "task_update_lock_all",
                       "tag_add",
                       "tag_update",
                       "user_read_all",
                       "user_update_assignment_weight",
                       "group_read_all",
                       "group_update"],
       "description": "superuser"},
      {"name": "user",
       "permissions": ["task_read",
                       "task_update_status",
                       "user_read",
                       "group_read"],
       "description": "user"}
    ]


def ensure_roles():
    db = models.db

    roles = {}
    print("* Creating roles")
    for role in ROLES_DEFINITION:
        if models.Role.query.filter(models.Role.name == role['name']).count() == 0:
            roles[role['name']] = user_datastore.create_role(**role)
            print(f"{roles[role['name']]} {role}")
        else:
            print(f"[WARNING] Skipping Role {role['name']}, already exists!")
    db.session.commit()


def ensure_admin_user():
    admin_username = os.environ.get('TASKMAN_ADMIN_USERNAME')
    admin_email = os.environ.get('TASKMAN_ADMIN_EMAIL')
    admin_password = os.environ.get('TASKMAN_ADMIN_PASSWORD')
    admin_force_update = os.environ.get('TASKMAN_ADMIN_FORCE_UPDATE')

    if not admin_username or not admin_email or not admin_password:
        print(f'Can only ensure admin account if TASKMAN_ADMIN_USERNAME, TASKMAN_ADMIN_EMAIL, and TASKMAN_ADMIN_PASSWORD are set')
        return

    admin_user = models.User.query.filter_by(username=admin_username).one_or_none()
    #print(f'Found admin user: {admin_user}')

    if admin_force_update and admin_user is not None:
        admin_user.password = admin_password
        admin_user.email = admin_email
        admin_user.active = True
        print('Updated admin user details.')
    elif admin_force_update or admin_user is None:
        admin_password = hash_password(admin_password)
        # We assume that the admin role ("admin") is created in the ensure_roles step in the onboarding flow.
        admin_user = user_datastore.create_user(
            username=admin_username,
            password=admin_password,
            name='Administrator Account',
            email=admin_email,
            active=True,
            roles=["admin"],
            confirmed_at=datetime.datetime.now(),
        )

        models.db.session.add(admin_user)
        print('Created admin user.')
    else:
        print('Admin user already exists, skipping creation.')

    if admin_user is not None:
        models.db.session.commit()