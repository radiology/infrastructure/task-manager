$(function() {
    // Fix the height of the #nav-lower-wrapper to avoid jumps when #nav-lower gets affixed.
    $('#nav-lower-wrapper').height($("#nav-lower").height() + 5);
});