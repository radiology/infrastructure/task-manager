#!/bin/bash

# Create config
[[ -z $DB_PORT ]] && export DB_PORT=5432
export SQLALCHEMY_DATABASE_URI="${DB_SCHEMA}://${DB_USER}:${DB_PASS}@${DB_HOST}:${DB_PORT}/${DB_NAME}"

# Create/Clean data base
echo "Waiting for database to initialize"
/root/wait-for-it.sh ${DB_HOST}:${DB_PORT} -t 600  #waiting for DB to initialize

get_host_from_url(){
  url=$1
  # extract the protocol
  proto="$(echo $1 | grep :// | sed -e's,^\(.*://\).*,\1,g')"

  # remove the protocol -- updated
  if [ ! -z $proto ]; then 
      url=$(echo $1 | sed -e s,$proto,,g)
  fi

  # extract the user (if any)
  user="$(echo $url | grep @ | cut -d@ -f1)"

  # extract the host and port -- updated
  hostport=$(echo $url | sed -e s,$user@,,g | cut -d/ -f1)

  # by request host without port
  host="$(echo $hostport | sed -e 's,:.*,,g')"
  # by request - try to extract the port
  port="$(echo $hostport | sed -e 's,^.*:,:,g' -e 's,.*:\([0-9]*\).*,\1,g' -e 's,[^0-9],,g')"

  # extract the path (if any)
  path="$(echo $url | grep / | cut -d/ -f2-)"

  echo $host
}

clone_project_repo(){
  PROJECT_REPO=$1
  echo "Cloning project ${PROJECT_REPO}"
  # Setting up private keys if info is there
  if [ -d /root/.ssh ] ; then
    echo 'Adding private keys from /root/.ssh'  
    eval $(ssh-agent -s)
    ssh-add ~/.ssh/*
  elif [ ! -z "${SSH_PRIVATE_KEY_BASE64}" ] ; then 
    echo "Adding base64 encoded private key from environment"
    mkdir -p ~/.ssh
    eval $(ssh-agent -s)
    echo "$SSH_PRIVATE_KEY_BASE64" | base64 -d | ssh-add -

  elif [ ! -z "$SSH_PRIVATE_KEY" ] ; then
    echo "Adding private key from environment"
    mkdir -p ~/.ssh
    eval $(ssh-agent -s)
    # echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add -
    echo "$SSH_PRIVATE_KEY" > ~/.ssh/id_repo_taskmanager_project
    chmod og-rwx ~/.ssh/id_repo_taskmanager_project
    cat ~/.ssh/id_repo_taskmanager_project
    ssh-add ~/.ssh/id_repo_taskmanager_project
  fi

  # Add project_repo server to known hosts 
  host=$(get_host_from_url ${PROJECT_REPO})
  echo "Adding ${host} to known hosts"
  ssh-keyscan ${host} > /root/.ssh/known_hosts
  
  CONFIG_PATH=/tmp/project
  # Clone project
  GIT_OPTIONS="--depth 1"
  if [ ! -z $PROJECT_BRANCH ] ; then
    GIT_OPTIONS="$GIT_OPTIONS --branch $PROJECT_BRANCH --single-branch"
  fi
  echo "Cloning with the following command: git clone $GIT_OPTIONS ${PROJECT_REPO} $CONFIG_PATH"
  git clone $GIT_OPTIONS ${PROJECT_REPO} $CONFIG_PATH
  
  # Move the configs to the designated location
  if [ ! -z $PROJECT_RELATIVE_PATH ] ; then
    CONFIG_PATH="$CONFIG_PATH/$PROJECT_RELATIVE_PATH"
  fi
  echo "Moving the cloned repo to the project location: mv $CONFIG_PATH /project"
  mv $CONFIG_PATH /project

  # Remove keys from environment and disc
  if [ -f ~/.ssh/id_repo_taskmanager_project ] ; then
    rm ~/.ssh/id_repo_taskmanager_project
  fi
  ssh-add -D
}

# Cloning project data
if [ ! -z "${PROJECT_REPO}" ] ; then
  clone_project_repo ${PROJECT_REPO}
fi

if [ -r "/project" ]; then
  echo "Using data in /project"
else
  echo "No Project data found"
  exit
fi

# config taskmanager
echo "initializing taskmanager database"
taskmanager-db-init
taskmanager-onboarding


# Start taskmanager
echo "Starting taskmanager"
[[ -z $TASKMANAGER_PORT  ]] && export TASKMANAGER_PORT=5000
[[ -z $GUNICORN_WORKER_CLASS ]] && export GUNICORN_WORKER_CLASS=gevent
[[ -z $GUNICORN_WORKER_CONNECTIONS ]] && export GUNICORN_WORKER_CONNECTIONS=1000
[[ -z $GUNICORN_TIMEOUT ]] && export GUNICORN_TIMEOUT=120
export GUNICORN_CMD_ARGS="--bind=0.0.0.0:${TASKMANAGER_PORT} --worker-class=${GUNICORN_WORKER_CLASS} --worker-connections=${GUNICORN_WORKER_CONNECTIONS} --timeout=${GUNICORN_TIMEOUT} --capture-output --access-logfile - --error-logfile -"
gunicorn --check-config 'taskmanager:create_app()'
gunicorn 'taskmanager:create_app()'

