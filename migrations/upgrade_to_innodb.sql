-- Change DB engine for all tables
ALTER TABLE `user` ENGINE=InnoDB;
ALTER TABLE `task_user_links` ENGINE=InnoDB;
ALTER TABLE `task_template` ENGINE=InnoDB;
ALTER TABLE `task_group_links` ENGINE=InnoDB;
ALTER TABLE `task` ENGINE=InnoDB;
ALTER TABLE `tag_task_links` ENGINE=InnoDB;
ALTER TABLE `tag` ENGINE=InnoDB;
ALTER TABLE `group_membership` ENGINE=InnoDB;
ALTER TABLE `group` ENGINE=InnoDB;

-- Add all foreign contraints
ALTER TABLE `group_membership` ADD CONSTRAINT `fk_group_membership_group_id_group` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`);
ALTER TABLE `group_membership` ADD CONSTRAINT `fk_group_membership_user_id_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
-- ALTER TABLE `roles_users` ADD CONSTRAINT `fk_roles_users_role_id_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);
-- ALTER TABLE `roles_users` ADD CONSTRAINT `fk_roles_users_user_id_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
ALTER TABLE `tag_task_links` ADD CONSTRAINT `fk_tag_task_links_tag_id_tag` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`);
ALTER TABLE `tag_task_links` ADD CONSTRAINT `fk_tag_task_links_task_id_task` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`);
ALTER TABLE `task` ADD CONSTRAINT `fk_task_lock_id_user` FOREIGN KEY (`lock_id`) REFERENCES `user` (`id`);
-- ALTER TABLE `task` ADD CONSTRAINT `fk_task_parent_id_task_group` FOREIGN KEY (`parent_id`) REFERENCES `task_group` (`id`);
-- ALTER TABLE `task` ADD CONSTRAINT `fk_task_template_id_task_template` FOREIGN KEY (`template_id`) REFERENCES `task_template` (`id`);
ALTER TABLE `task_group_links` ADD CONSTRAINT `fk_task_group_links_group_id_group` FOREIGN KEY (`group_id`) REFERENCES `group` (`id`);
ALTER TABLE `task_group_links` ADD CONSTRAINT `fk_task_group_links_task_id_task` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`);
ALTER TABLE `task_user_links` ADD CONSTRAINT `fk_task_user_links_task_id_task` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`);
ALTER TABLE `task_user_links` ADD CONSTRAINT `fk_task_user_links_user_id_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);
