"""Removes unique constraint from task_group.label

Revision ID: 5cc57ed5145f
Revises: aa63e45787cd
Create Date: 2020-11-20 11:30:23.868985

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5cc57ed5145f'
down_revision = 'aa63e45787cd'
branch_labels = None
depends_on = None


def upgrade():
    # Removing the unique constrain on task_group.label

    # Because the unique contraint on task_group.label is not NAMED, we need to apply a work-around to remove it.
    # First define a naming convention for unique contraints:
    naming_convention = {
        "uq": "uq_%(table_name)s_%(column_0_name)s",
    }

    # With the batch_alter_table method a naming_convention can be given, this magically also makes an 
    # unnamed constrained adhere to the naming convention, so we are able to to drop it (JEEJ).
    with op.batch_alter_table("task_group", naming_convention=naming_convention) as batch_op:
        batch_op.drop_constraint("uq_task_group_label", type_="unique")



def downgrade():
    # This adds a unique constraint to label again. This is not really tested, but should work on anything else than SQLite
    op.create_unique_constraint("uq_task_group_label", "task_group", ['label'])
