# Running a local kubernetes cluster

For trying out the Medical Imaging Infrastructure you need to have access to a kubernetes cluster. You can easily do this using k3d.

1. Install k3d: https://k3d.io
2. Setup your own local registry: `k3d registry create infra-dev-registry.localhost --port 15000`
3. Create a cluster: `k3d cluster create --config demo-cluster-config.yaml`
4. Install the infra: `helm ...`


# Cleaning up
1. `k3d cluster delete medical-imaging-demo`
2. `k3d registry delete k3d-infra-dev-registry.localhost`

